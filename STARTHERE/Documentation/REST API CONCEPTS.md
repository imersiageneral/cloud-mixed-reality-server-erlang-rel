# REST API CONCEPTS

[TOC]

The MR Server API is based on the cowboy webserver[[1\]](applewebdata://1E6D5DF8-2C95-4373-AB33-970E9F433302#_ftn1), using best principles of RESTful[[2\]](applewebdata://1E6D5DF8-2C95-4373-AB33-970E9F433302#_ftn2)APIs.

## COMMON HEADERS

The API is divided into groups of commands arranged around the core entities and concepts. Each command, except some of the user commands, requires headers of:

- **developerid**: A reverse DNS ID as set in the license for this mrserver.  For example, "com.imesia.webapp".

- **location**: Each time a command is issued, the current user / device location is required as well.  This is set as a Geohash.

- **sessionid**: The current SessionID obtained from a process of identification (eg logging in).

- **contextids**: The contextIDs allow users extra capabilities over Channels and Geobots.  Can either be a single ID, or a JSON array of IDs.

These common headers won’t be mentioned in the descriptions below, except where they are not needed – otherwise assume they have to be there for the API command to work.

## COMMANDS

Commands are defined as a combination of the URI (the bit after the base address), the method (HEAD, OPTIONS, GET, POST, PUT), headers (key /value pairs), URL parameters (key/value pairs after the ?) and sometimes the body (for PUT and POST).  So, for example, to get a list of Geobots in a channel:

- URI – /api/geobots

- Method – GET

- Headers –

  - developerid = whatever your DeveloperID is,

  - channelid = the Channel to find the Geobots in,

  - sessionid = a currently valid sessionid for this logged-in user,

  - showhidden = true or false to show the hidden Geobots on Channels you own,

  - location = position to look out from,

  - radius = distance around the point within which to look.

The MR Server can then respond appropriately with a response number, and if appropriate a message or piece of data, for example:

- Response – 200 – OK

- Response body – a JSON array of Geobots.

The methods used complies with the standard for APIs, thus:

- HEAD – find out whether a resource exists.
- OPTIONS – find out what methods are valid.
- GET – Get details of a resource (or sometimes a list of resources).
- POST – Create a new resource.
- PUT – Update an existing resource.
- DELETE – Delete a resource.

## RESPONSE VALUE MEANINGS

In the REST convention, certain response values have specific meanings; these are:

- 200 – All OK; often, additional response details are included in the body.
- 204 – No content; Also signifies a successful outcome, but no additional information is being sent.
- 401 – Unauthorized; usually due to an incorrect DeveloperID, or no location.
- 404 – Not found; the resource being requested or addressed does not exist.
- 415 – Unsupported Media Type; The body has to be application/json media type or multipart/file when doing a POST or PUT.
- 500 – Internal Error; something went wrong in the system.  Possibly a parameter was incorrectly set and couldn’t be interpreted (eg incorrectly formatted or invalid JSON).

## COMMON 404 ERRORS

Some ’resource not found’ error responses are common across commands, these are:

- {"error", "sessionid"} – the SessionID was invalid or not sent.
- {"error", [header name]} – the Header given was invalid in some way, or not present.  For example {"error", "channelid"} indicates the ChannelID supplied was invalid, or that a ChannelID wasn’t supplied.
- {"error", "database"} – this suggests that some internal database error has occurred.  Best to contact your system administrator.

## EXAMPLES

### JQUERY AND AJAX

The following example in Javascript shows how a typical GET operation can be coded using jquery AJAX commands.
```js
IMERSIA.SDK.prototype.GetFromAPI = function ( sessionid, command, parameters, callback, errorcallback )
{
   var self = this;
 
   parameters.location = self.currentlocation;
   parameters.developerid = self.developerid;
   parameters.sessionid = sessionid;
 
   $.ajax({
      url : '/api/' + command,
      headers : parameters,
      type : 'GET',
      processData: false,
      dataType: "json",
      cache: false,
      success : function(data) {
         if (callback) callback(data);
      },
      error : function (data) {
         if (errorcallback) errorcallback(data.responseJSON);
      }
   });
};
```
### REACT

The following example shows how a typical GET operation as used in the imersiasdk.js file for React.js.

```typescript
GET = (command, parameters, passthrough = {}) =>
{
	return new Promise ( (resolve, reject) =>
	{
		let status = 0;
		let headers = parameters;
		if (this.sessionid) headers.sessionid = this.sessionid;
		if (!headers.location) headers.location = this.headers.location;
		headers.developerid = this.headers.developerid;

		// Get the item requested
		fetch("https://" + window.location.host + "/api/" + command, { method:"GET", headers:headers })
		.then ((resp) => {
			status = resp.status;
			return (resp.json());
		})
		.then((resp) => {
			if (status === 200 || status === 204)
			{
				resolve ( {"result": ImersiaSDK.OK, "status": status, "json": resp, "passthrough": passthrough} );
			}
			else
			{
				reject ( {"result": ImersiaSDK.ERROR, "status": status, "json": resp, "passthrough": passthrough} );
			}
		})
		.catch ((err) => {
			reject ( {"result": ImersiaSDK.ERROR, "status": 500, "json": {"error": "get"}, "passthrough": passthrough } );
		});
	});
}
```

### PYTHON

A GET function in Python could look like this using the requests library:
```python
def GetCommand (command, headers):
   headers.update({'sessionid' : sessionid, 'developerid' : developerid, 'location': geohash})
   req = requests.get(apiurl + "/" + command, headers=headers, verify=False)
   if ((req.status_code == 200) or (req.status_code == 204)):
      return {'status':req.status_code, 'result':json.loads(req.text)}
   else:
      return {'status':req.status_code, 'result':{}}
```
### UNITY3D C\# 

A GET function in C\# as used in unity3D:

```csharp
public void GET(string command, Hashtable headers, ImersiaCall callback = null, object passthrough = null, bool useSessionID = true) =>
    StartCoroutine(_GET(command, headers, callback, passthrough, useSessionID));

private IEnumerator _GET(string command, Hashtable headers, ImersiaCall callback, object passthrough, bool useSessionID)
{
    UnityWebRequest wr = UnityWebRequest.Get("https://" + serverAddress + "/api/" + command);
    wr.certificateHandler = new AcceptAllCertificates();

    if (headers != null)
    {
        foreach (string key in headers.Keys)
        {
            wr.SetRequestHeader(key, (string)headers[key]);
        }
    }

    wr.SetRequestHeader("developerid", developerid);
    wr.SetRequestHeader("location", location);
    if (useSessionID) wr.SetRequestHeader("sessionid", sessionid);

    yield return wr.SendWebRequest();

    if (wr.isNetworkError)
    {
        Debug_Log("Error while doing GET: " + wr.error);
        object response = JSON.JsonDecode(wr.downloadHandler.text);
        callback?.Invoke(wr.responseCode, response, passthrough);
    }
    else
    {
        object response = JSON.JsonDecode(wr.downloadHandler.text);
        callback?.Invoke(wr.responseCode, response, passthrough);
    }
}
```

### XCODE SWIFT

A GET function in swift for an iOS App.

```swift
func GET (command: String, headers: NSDictionary? = nil, callback: Callback? = nil, passthrough: NSDictionary? = nil)
{
	// Set the core elements
	var components = URLComponents()
	components.scheme = "https"
	components.host = ImersiaSDK.serverAddress
	components.path = "/api/" + command
	let url = components.url
	
	if (url == nil)
	{
		print ("Error creating URL")
		if (callback != nil) { callback! (400, [:], passthrough) }
	}
	else {
		var request = URLRequest(url: url!)
		let session = URLSession.shared
		request.httpMethod = "GET"
		
		// Set the headers from the parameters in
		if (headers != nil) {
			for (key, value) in headers! {
				request.addValue (value as! String, forHTTPHeaderField: key as! String)
			}
		}

		// Set the standard headers
		if (sessionid != nil) { request.addValue(self.sessionid!, forHTTPHeaderField: "sessionid") }
		request.addValue(self.developerid, forHTTPHeaderField: "developerid")
		request.addValue(self.location, forHTTPHeaderField: "location")
		
		// Perform the request
		let task = session.dataTask(with: request, completionHandler: {data, response, error -> Void in
			do {
				if (data != nil)
				{
					let httpResponse : HTTPURLResponse = response as! HTTPURLResponse
					let dataJSON : NSDictionary = try JSONSerialization.jsonObject(with: data!) as! NSDictionary
					if (callback != nil) {
						callback! (httpResponse.statusCode, dataJSON, passthrough)
					}
				}
				else {
					if (callback != nil) { callback! (400, [:], passthrough) }
				}
			}
			catch
			{
				if (callback != nil) { callback! (400, [:], passthrough) }
			}
		})
		
		task.resume()
	}
}
```



------

[[1\]](applewebdata://1E6D5DF8-2C95-4373-AB33-970E9F433302#_ftnref1)https://ninenines.eu

[[2\]](applewebdata://1E6D5DF8-2C95-4373-AB33-970E9F433302#_ftnref2)https://www.ics.uci.edu/~fielding/pubs/dissertation/rest_arch_style.htm