# DEFAULT WEBAPPS

The MRServer comes with two webapps built in, one for basic admin capabilities (in the https folder) and one for basic card-style interaction with the MRServer (in the http folder). These of course can be replaced by whatever you like, or modified to suit. These webapps are built in React[[1]](applewebdata://2DF3389C-1E6D-46F7-B17A-FF6359C026A0#_ftn1)using SemanticUI[[2]](applewebdata://2DF3389C-1E6D-46F7-B17A-FF6359C026A0#_ftn2). For the source-code, please contact us at [mrserver@imersia.com](mailto:mrserver@imersia.com).

## THE ADMIN SITE

To enter the admin site, go to <https://your_mrserver_url/admin> If you have set httpsdefault = "admin" in the mrserver.toml file, then [https://your_mrserver_url](https://your_mrserver_url/) will take you straight there. This is a comprehensive WebApp, giving intuitive access to most of the MRServer's capabilities including creating, modifying deleting Channels and Geobots; setting metadata; loading files; setting contextids, programming Geobots, viewing analytics – and much more.

If the user logged in is the SuperAdmin, then they can also administer all the other content and users on the MRServer.

![Figure3](./Figure3.png)

Figure 3: Editing Geobots on the MRServer admin site.

![Figure4](./Figure4.png)

Figure 4: Dropdown menus for editing users, channels and geobots showing the range of possibilities.

![Figure5](./Figure5.png)

Figure 5: Editing Geobots on the map view – here you can drag them to the desired location.

![Figure6](./Figure6.png)

Figure 6: Analytics views, as a graph or a heatmap.

![Figure7](./Figure7.png)

Figure 7: Programming a Geobot.

## THE CARDS SITE

The cards site gives basic access to publicly viewable Geobots. The entry view shows a set of Channels. Clicking on a Channel card opens a list of Geobots on that Channel. Geobots will show commands available from the Automations, and play sound if appropriate. Clicking on the Geobot card will send a 'touch' event to the Geobot's Automations.

To enter the cards site, go to [http://your_mrserver_url/default](http://your_mrserver_url/defaultn) If you have set httpdefault = "default" in the mrserver.ini file, then [http://your_mrserver_url](http://your_mrserver_url/) will take you straight there.

![Figure8](./Figure8.png)

Figure 8: Viewing Geobots on a Channel in the cards site.

--------------------------------------------------------------------------------

[[1]](applewebdata://2DF3389C-1E6D-46F7-B17A-FF6359C026A0#_ftnref1)<https://reactjs.org/>

[[2]](applewebdata://2DF3389C-1E6D-46F7-B17A-FF6359C026A0#_ftnref2)[https://react.semantic-ui.com](https://react.semantic-ui.com/)
