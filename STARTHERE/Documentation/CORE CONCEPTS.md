# CORE CONCEPTS

[TOC]

## COMPANIONS

Each user has a companion agent, as defined by their UserID. When connected to the MR Server, all communications are handled by the companion agent. Companions are running even when a user isn't connected to the MR Server.

## DIGITAL AGENTS (GEOBOTS)

The central concept of the MR Server is the geospatial digital agent. The Companion is the user's digital agent, whereas gamified elements in the real world are represented by Geobots. In the future, there will be other forms of digital agent. Geobots are classed as semi-embodied digital agents, according to the generally accepted definition[[1]](applewebdata://EC58109D-9DFC-4869-A331-1FB688E8372C#_ftn1).

## DIGITAL ENTITIES

Any construct that has an independent definition in the MR Server is a digital entity. Presently, digital entities include Companions, Channels and Geobots. Entities generally have a base set of characteristics (such as a name, description and ID), and can hold files and metadata. Each entity 'executes' on the MR Server, meaning it is able to monitor activities related to itself and other associated entities. Therefore, a Companion monitors Channels and a Channel monitors Geobots.

## COLLECTIONS (CHANNELS)

Each type of digital agent is grouped together in a collection, for example, Geobots are collected into Channels. Channels are entities too, and have their own properties to form the basis for 'an experience' which mobile device users can connect to. A user can monitor a Channel in order to be made aware when any of the Geobots on the Channel change.

## METADATA

Each digital agent and collection can hold additional metadata – key / value pairs that can represent any type of textual or numerical content. Oftentimes, a JSON format is used to represent more complex data structures. Metadata is used by digital agents in their programs to store intermediate values, gather data, etc.

## FILES AND STREAMING

Any digital entity can hold files. These can be any type of file, but certain formats are treated specially. For example, images can be pre-processed on the MR Server to reduce file size, whilst audio and video files are streamed. Some files need to be downloaded to the device in their entirety before being used (eg a pdf file), whereas some can be loaded in segments depending on user actions (eg a 3D model that changes its level of detail based on proximity to the viewer).

## CONTEXTS

By default, all Channels and Geobots are viewable by any other logged-in user via the API, and by the general public via the GeoJSON API commands. However, both can be 'locked down' by setting the hidden parameter to true. A Geobot or Channel that is hidden can only be found, viewed, wotched, programmed, have files loaded and read, and so forth, by its owner.

However, there are situations when some of these capabilities need to be given to others so that they can find, read, wotch, use and potentially change the Channel or Geobot. To achieve this, the MRServer uses ContextIDs. A ContextID can be considered a key that unlocks certain capabilities for the key holder to hidden Channels or Geobots.

Each Channel or Geobot can be allocated any number of ContextIDs, and these are then associated only with that entity. These are passed in the headers for an API command using the key 'contextids'. The value can be a JSON array or a single contextid.

## PROGRAMMABILITY

Digital agents can be programmed to respond to users, each other, and environmental factors. See the appropriate sections that covers this in more detail.

## WOTCHA

The websockets allow devices to monitor activities on Geobots and Channels in realtime. However, they don't want to be inundated with information about everything that is going on in the MR Server. Instead, they express interest in the Geobots or Channels they want to monitor by issuing a 'wotcha', where-after, they are kept up-to-date about changes and events.

When 'wotching' a Geobot, only changes about that Geobot are relayed, whereas when 'wotching' a Channel, changes to the channel, including the Geobots on that channel, are relayed. A Companion can 'wotch' multiple Channels and Geobots at the same time. It should be noted that it is the Companion that is wotching, so any devices connected to that Companion will receive the same notifications.

## EVENTS

Events are 'things that happen', sometimes with an accompanying payload of data. These are noticed by the digital agents and can then affect the response they make. Events can be initiated by users, sensors in the world, other digital agents, and other digital systems. Events may be timed to occur at specific times, or in response to environmental factors, such as, for example, the number of people nearby.

## TRIGGERS

Digital agents can trigger mobile devices, webapps or actuators in the real world to 'do something'. This is used to synchronize sounds, animations or indeed any other form of shared user experience in a location. Triggers are only received by devices that have registered as 'wotching' a digital agent or collection.

## ENTANGLEMENT

A concept related to wotcha is 'entanglement'. A Geobot on one MRServer can become 'entangled' with a Geobot on another (remote) MRServer. This means that when the remote Geobot issues a trigger event, this is received on the local Geobot as an event along with the trigger parameters. Likewise the local Geobot can send an event with parameters to the remote Geobot once it is entangled.

## ANALYTICS

A core feature of the MR Server is the ability to gather analytics related to the activities of people and digital entities. Analytics are collated by event, into hourly steps, and an 8 character GEOHASH location.

## PASSCODES

MRServers can issue One Time Passcodes to be used for a variety of purposes.  A passcode can be issued to any email address, and is sent on an encrypted channel to that email address.  Each passcode is a 6-character string, and is valid for three minutes only, after which it expires.  Passcodes keep a track of how many times they have been attempted, with three tries only being allowed.  As the name suggests, once a one-time-passcode has been used, it becomes invalid.

Passcodes ares used for:

- Registration in order to confirm ownership of the email address.
- Changing password to confirm that the user with the email address intended to change the password.
- Logging in instead of passwords when authentication is set to 'passcode' in the mrserver.toml file.
- Logging in as well as passwords when authentication is set to 'twofactor' in the mrserver.toml file.

## BILLING (ALPHA)

An MR Server can be connected to a billing engine so that users can be charged for services. For example, you might charge a user for consuming a geospatial story, scanning an AR Marker, or taking part in a game.

Presently, the MR Server uses a service called 'billingfox' ([www.billingfox.com](http://www.billingfox.com/)). If you are setting up an MR Server for yourself, and want to charge for services, you will need to create a billingfox API Key, and enter this in the mrserver.toml file. Billingfox then takes care of the complicated process of managing payments from cards, sending emails to people when their account needs topping up. The MRServer provides tools to enable you to charge people for services when desired (see the spend action in the section about programming Geobots).

The billing process works well, but is still in alpha pending further consideration about how people (like yourselves) will want to use it in different situations.

UPDATE Dec 2019 - The billingfox service has been unstable and oftentimes unavailable, so this will be changed soon.

## LOGFILE.TXT

Companions, Channels and Geobots write errors to a file called 'logfile.txt'.  If you want to see the debug messages as well as the error messages, set a metadata with the key "debug" to the value "true".  Note that with debugging on, the logfile can get quite large, so best to turn it off when no longer required.

# INSIDE AN MR SERVER

Each entity inside an MR Server is interconnected, and communicates with each other. Whether communicating via the API or Websockets, the user's Companion is always the first point of contact. This in turn communicates with the other entities on behalf of the user. Note that Channels are live entities as well, and work to gather events and communications between Geobots and Companions.

![Figure1](./Figure1.png)

Figure 1: Two users with multiple devices 'wotching' the same Channels and Geobots.

--------------------------------------------------------------------------------

[[1]](applewebdata://EC58109D-9DFC-4869-A331-1FB688E8372C#_ftnref1)<https://en.wikipedia.org/wiki/Embodied_agent>