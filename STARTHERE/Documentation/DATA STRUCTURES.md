# DATA STRUCTURES

[TOC]

All entities are represented by JSON[[1\]](applewebdata://B151ECD9-2EA6-468C-ADAD-3D117658CD69#_ftn1)data-structures, which are used in the body and responses of API commands, and as part of the websocket messages.

In the more complex data structures, such as GEOBOT and CHANNEL, the DATE parameters ’created’ and ’modified’ are sent when reading from the MR Server, but are not required to be filled in when sending data to the MR Server.

In the definitions below, there may be arrays of data.  Whilst these are written in this documentation as [ ITEM, ITEM, … ], this should be interpreted to mean: An array of zero to n ITEMs, thus an empty array [ ] is also valid.

Where a value is defined with vertical lines between datatypes, for example: number | string | boolean, this should be interpreted to mean 'or'; ie that any one of these datatypes is valid.

## BASIC TYPES

Standard JSON basic types are supported, namely: string, number, object, array, boolean and null.  Additional complex types and data structures are also supported, as detailed below.

## GUID

All entities have a unique ID in the form of a GUID[[2\]](applewebdata://B151ECD9-2EA6-468C-ADAD-3D117658CD69#_ftn2).  This is a 40 character string which identifies this entity uniquely in the world. For example: "23435f38-b3e6-11e8-a4d4-e7e19c263eab".  Any variable or parameter names in the MR Server ending in ’id’ is a GUID.  Using a globally unique ID ensures that digital entities can hop from one server to another without conflicting with other digital entities.

## DATE

All dates are in ISO8601 format[[3\]](applewebdata://B151ECD9-2EA6-468C-ADAD-3D117658CD69#_ftn3), and stored at UTC 0, so a date conversion will need to be done to relate this to the local date and time zone.  An example date is: "2018-09-28T00:49:20Z".

## GEOHASH

Location information is sometimes communicated using a Geohash[[4\]](applewebdata://B151ECD9-2EA6-468C-ADAD-3D117658CD69#_ftn4), for example, the location header in the API. A geohash is an encoded form of latitude and longitude which can also be used in URLs, for example: "GBSUV7ZTQ".

## CLASS

Channels and Geobots can be classified to further refine their capabilities and behaviour using a reverse DNS notation[[5\]](applewebdata://B151ECD9-2EA6-468C-ADAD-3D117658CD69#_ftn5), for example "com.imersia.default". In Channels, this defines the type of experience (for example a treasure hunt or photo map), whereas in Geobots, this defines the type of content and interaction (for example, an animated sprite or 3D object).  How a class of object is represented is determined by the viewing APP or website, and whether it takes note of the class designation.

### CHANNEL CLASSES

NB: As of writing, only the Default and Geophoto classes are functional.

- COM.IMERSIA.DEFAULT– A normal Channel with a generic experience

- COM.IMERSIA.GEOPHOTO– A geo-photo library consists of images located at places, sometimes with narrative, music or extra information. 

- COM.IMERSIA.STORYLINE– A storyline is a trail through a place, accompanied by a "walking narrative". It may be augmented with images and videos if desired. 

- COM.IMERSIA.VIRTUANDIZE– Augment any image with 3D content, videos, links and sound. Can be used with posters, t-shirts, mugs, caps - in fact any printed media.

- COM.IMERSIA.TREASUREHUNT– Create a linked series of clues that automatically records who has been at each clue, and manages prizes for those who complete the hunt.

- COM.IMERSIA.CONTEXTAD– Contextual Advertising allows you to use AI to target a specific message to a potential client when they are most receptive to your product or service. 

- COM.IMERSIA.GEOREMINDER– A Channel for holding reminders for a specific time and place. Add documents relevant to then and there. For example, a shopping list for when you reach the supermarket, or a reminder of where you left the car.

### GEOBOT CLASSES

NB:  As of writing, only the default and sprite classes are functional.

- COM.IMERSIA.DEFAULT– a normal Geobot seen as a marker in the viewer.

- COM.IMERSIA.SPRITE– an animated sprite.

- COM.IMERSIA.PANORAMA– a 360 degree photographic panorama image.

- COM.IMERSIA.THREESIXTYVIDEO– a 360 degree video.

- COM.IMERSIA.VR– a VR world or animated character.

- COM.IMERSIA.IMAGEMARKER– a image that is used in AR to activate a video.

### CLASS METADATA

Typically, a class is further refined using a metadata entry for the Channel or Geobot with its key matching the class name.  For example, the com.imersia.sprite Geobot class uses the metadata with the same key, with the value set as:
```js
{
   "touch": "touch",
   "imageurl": "explosion.png",
   "playonstart": false,
   "repeat": false,
   "trigger": "go",
   "scale": 1,
   "width": 7,
   "height": 7,
   "length": 49,
   "framerate": 30
}
```
## SESSION

A sessionid is returned when logging in, which is then used in all communications with the API (unless communicating anonymously, as with the GeoJSON commands).
```js
{
   "sessionid": GUID
}
```
## USER

The full user record includes not only the User Details, but also the user’s id and when they were created and modified.  Note that the password field is always left empty when requesting user details.  Any other user parameters and details are stored in the Companion Metadata for that user.
```js
{
   "userid": GUID,
   "useremail": string,
   "password": string,
   "details": USER_DETAILS,
   "location": LOCATION,
   "created": DATE,
   "modified": DATE
}
```
## USER_DETAILS
```js
{
   "firstname": string,
   "surname": string,
   "nickname": string
}
```
## CHANNEL

Most of the features of a Channel are self-explanatory.  OwnerID is the UserID of the user who created and thus owns the Channel.  One that needs clarifications is the term ’hidden’, which is used to define whether other users apart from the owner of the Channel, can see it in lists.  Therefore, a Channel with the hidden feature set to true, cannot be seen by the general public (ie people other than its owner).
```js
{
   "channelid": GUID,
   "ownerid": GUID,
   "name": string,
   "description":string,
   "class": CLASS,
   "imageurl": string,
   "hidden": boolean,
   "created": DATE,
   "modified": DATE
}
```
## LOCATION

All Geobots and Companions have a location in three dimensions, using the standard Web Mercator projection[[6\]](applewebdata://B151ECD9-2EA6-468C-ADAD-3D117658CD69#_ftn6), with altitude.  Latitude lines run east-west and are parallel to each other. If you go further north, latitude values increase.  Latitude values (Y-values) range between -90 and +90 degrees. Longitude lines, however, run north-south and converge at the poles.  These are the X-coordinates ranging between -180 and +180 degrees. Altitude is measured in meters above sea level.  The Geohash returned is always consistent with the latitude and longitude, and calculated to 12 characters.
```js
{
   "latitude": number,
   "longitude": number,
   "altitude": number,
   "geohash": GEOHASH
}
```
## GEOBOT

Most of the features of Geobots are self explanatory.  Similar to Channels, Geobots have a ’hidden’ parameter, however, they also have a radius.  When looking for Geobots that have a non-zero radius, you have to be within its radius to be able to find it.  This is useful, for example, in treasure hunts so people can’t cheat – they have to be nearby the Geobot to find it.  If a Geobot has its hidden parameter set to true, or is on a Channel that is hidden, then you also have to be its owner to be able to find it when searching by location.  A Geobot or Channel can always be found by its owner when listing all Channels owned by that user or Geobots on a Channel owned by that user.
```js
{
   "geobotid": GUID,
   "channelid": GUID,
   "ownerid": GUID,
   "name": string,
   "description": string,
   "class": CLASS,
   "imageurl": string,
   "location": LOCATION,
   "radius": number,
   "hidden": boolean,
   "created": DATE,
   "modified": DATE
}
```
## METADATA

All entities (Companions, Channels and Geobots) can have metadata.  Even though metadata is often referred to via its key, the metadataID is useful when changing the key of the metadata, whilst retaining its value.  A metadata key can be any combination of characters, numbers and special characters. A value can be a number, a string, a boolean (true or false), or a JSON array or object.
```js
{
   "metadataid": GUID
   "key": string,
   "value": string | number | boolean | array | object
}
```
## AUTOMATION

Automations, transitions and actions define the way Geobots can be programmed, and how they will respond to events.  See the section on programming Geobots for more information.
```js
{
   "automationid": GUID,
   "name": string,
   "description": string,
   "commands": [
      string, string, ...
   ],
   "transitions": [
      TRANSITION, TRANSITION, ...
   ]
}
```
## TRANSITION
```js
{
   "state": string,
   "event": string,
   "newstate": string,
   "actions": [
      ACTION, ACTION, ...
   ]
}
```
## ACTION
```js
{
   "command": string,
   "parameters": object
}
```
## ANALYTIC

An analytic record is a structure used to return information about analytics events. Analytics are summarized when stored and the location and time approximated, such that the date is rounded to the hour, whilst the Geohash is rounded to 8 characters.  This means that a single record counts the number of events of the same type that occur within that hour and approximate location, thus reducing the amount of storage required.

The parameters field contains the stored JSON parameters sent in when the analytics event was logged, one for each event instance.  The indexes field contains integers that identify the position of the parameters in a sparse array.  Empty parameters are not recorded, so any indices not in the list would have had no data.
```js
{
   "created": DATE,
   "geohash": GEOHASH,
   "event": string,
   "tally": number,
   "parameters": [
      array | object, array | object, ...
   ],
   "indexes": [
      number, number, ...
   ]
}
```
## CONTEXT

A context is an array of strings which designate the capabilities being unlocked. For example:
```js
["read", "edit", "log", "send", "list", "add", "remove"]
```
For Channels, the capabilities that can be unlocked are:

- Read – able to get the Channel's details.

- List – able to list Geobots on the Channel.

- Add – able to add Geobots to the Channel.

- Remove – able to remove Geobots from the Channel.

For Geobots, the capabilities that can be unlocked are:

- Read – able to get the Geobot's details.

- Edit – able to set Geobot's details, metadata, analytics and files.

- Log – able to send an event to be logged by the analytics system.

- Send – able to send an event to a Geobot's Automations.

## FILE

Some of the File and Maptile API commands return files rather than JSON structures. This is specified in the appropriate sections.

## GEOJSON

GeoJSON[[7\]](applewebdata://B151ECD9-2EA6-468C-ADAD-3D117658CD69#_ftn7)is a common format in communicating geospatial constructs, oftentimes used with mapping.  The MR Server API can be used to anonymously enquire about Channels and Geobots that are publicly viewable (ie, not hidden).  See the GeoJSON API commands for more information.

### CHANNEL_GEOJSON

The Channel_GeoJSON object is classed as a featurecollection, with the details being equivalent to the CHANNEL datastructure, with the addition of metadata, and the features representing the current Geobots on the Channel at that time.
```js
{
   "type": "FeatureCollection",
   "details": {
      "channelid": GUID,
      "ownerid": GUID,
      "name": string,
      "description": string,
      "class": CLASS,
      "imageurl": string,
      "hidden": boolean,
      "created": DATE,
      "modified": DATE,
      "metadata": [
         METADATA, METADATA, ...
      ]
   },
   "features": [
      GEOBOT_GEOJSON, GEOBOT_GEOJSON, ...
   ]
}
```
### GEOBOT_GEOJSON

The Geobot_GeoJSON object is classed as a GeoJSON feature object.  Note that the Geobot’s location is stored in two places in this data structure in order to comply with a Geobot being described as a GeoJSON Point as well as having properties analogous to a Geobot data structure.  The Properties field corresponds roughly to a GEOBOT data structure, with the addition of an array of Metadata.
```js
{
   "type": "Feature",
   "geometry": {
      "type": "Point",
      "coordinates": [
         number, // latitude
         number, // longitude
         number // altitude
      ]
   },
   "properties": {
      "geobotid": GUID,
      "channelid": GUID,
      "ownerid": GUID,
      "name": string,
      "description": string,
      "class": CLASS,
      "imageurl": string,
      "hidden": boolean,
      "location": LOCATION,
      "created": DATE,
      "modified": DATE
      "metadata": [
         METADATA, METADATA, ...
      ],
      "commands": [
         string, string, ...
      ]
   }
}
```

------

[[1\]](applewebdata://B151ECD9-2EA6-468C-ADAD-3D117658CD69#_ftnref1)[http://www.json.org](http://www.json.org/)

[[2\]](applewebdata://B151ECD9-2EA6-468C-ADAD-3D117658CD69#_ftnref2)https://en.wikipedia.org/wiki/Universally_unique_identifier

[[3\]](applewebdata://B151ECD9-2EA6-468C-ADAD-3D117658CD69#_ftnref3)https://en.wikipedia.org/wiki/ISO_8601

[[4\]](applewebdata://B151ECD9-2EA6-468C-ADAD-3D117658CD69#_ftnref4)https://en.wikipedia.org/wiki/Geohash

[[5\]](applewebdata://B151ECD9-2EA6-468C-ADAD-3D117658CD69#_ftnref5)https://en.wikipedia.org/wiki/Reverse_domain_name_notation

[[6\]](applewebdata://B151ECD9-2EA6-468C-ADAD-3D117658CD69#_ftnref6)https://en.wikipedia.org/wiki/Web_Mercator_projection

[[7\]](applewebdata://B151ECD9-2EA6-468C-ADAD-3D117658CD69#_ftnref7)http://geojson.org/