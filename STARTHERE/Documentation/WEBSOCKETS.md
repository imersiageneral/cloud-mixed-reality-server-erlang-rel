# WEBSOCKETS

[TOC]

## INTRODUCTION

A websocket is a continuous and open communication between the user’s device and the MR Server.  It allows mobile devices and websites to respond immediately to changes in Geobots and Channels without having to poll, thus significantly reducing communication overhead.  The process is as follows:

1. Log in using the API to get a SessionID.

2. Connect a websocket using the SessionID and the user’s UserID.  This connects to the user’s Companion.  The user can have multiple devices connected to their Companion at the same time, which means all the devices are kept in sync.

3. Specify which Channels or Geobots to watch.  These are called a ’wotchas’.  People’s devices wotching the same Channels and Geobots are thus also kept in sync.
4. Send events and commands through the WebSocket and Companion to specific Geobots.
5. Interpret and perform actions based on ’wotcha’ messages from the MR Server – these are

    a. Changes in Geobots or Channels (including deletion and creation)

    b. Triggers

    c. Other messages

6. At regular intervals, the MR Server sends a ’ping’ message to determine if the mobile device is still connected and responding.  The device must respond with a ’pong’ message or the connection will be closed.  This can continue indefinitely as long as the ping is answered with a pong.  If a websocket gets closed, perhaps due to the network being offline for a while, the device can always re-open it and should then re-issue ’wotcha’ requests.

## CONNECTING

To open a websocket, use the URL: wss://[mrserver address]/wotcha/[userid] with an appropriate websocket library.

For example, in typescript:
```typescript
let websocket = new WebSocket("wss://imersia.org/wotcha/" + userid);
```
## WOTCHING

To wotch a Channel or Geobot (from the English expression: ’wotcha’), send the following command (eg. in typescript) (where the sessionid and entityid (channelid or geobotid) are acquired from the API).  Note that when specifying a 'wotcha', this applies to the Companion and is active across all connections, not just this one.  So, if one connection wotches a specific channel, then all connections will be notified about that channel from then on.
```typescript
let command = {
   command : 'wotcha',
   sessionid : sessionid,
   parameters : { id : entityid }
};
websocket.send( JSON.stringify( command ) );
```
To stop wotching a Channel or Geobot, send the following command.  This stops notifications from the Entity.
```typescript
let command = {
   command : 'close',
   sessionid : sessionid,
   parameters : { id : entityid }
};
websocket.send( JSON.stringify( command ) );
```

## RECEIVING

Receiving messages from a websocket is set up once the websocket is open, for example:
```typescript
websocket.onmessage = function( evt ) {
if ( evt.data == 'ping' )
   {
      // Respond to the ping message
      websocket.send( 'pong' );
   }
else
   {
      // Do something with the data
      let message = JSON.parse( evt.data );
      if ( message.hasOwnProperty( "wotcha" ) )
      {
         if (message.wotcha.hasOwnProperty( "trigger" ))
         {
            // This is a trigger
            let triggering_geobot = message.wotcha.trigger.geobotid;
            // Do something with the Trigger for this Geobot
            ...
         }
         else
         {
            // This is an update event
         }
      }
   }
};
```
## NOTIFICATIONS

The websocket sends notification of any changes in the details, metadata, programming and creation / deletion of users, Geobots or Channels.  These are classified in much the same way as for the API.

### USER

#### USER_SET_DETAILS

The details of the user have been changed.  The message contains the full user details as updated, for example:
```js
{
   "wotcha": {
      "userid": GUID,
      "user_set_details": {
         "details": USER_DETAILS
      }
   }
}
```
Note that the user’s email address and password are not sent since the former cannot be changed and the latter is, of course, kept secret.  No notification is sent out if the password is changed.

#### USER_METADATA

A metadata value on the user’s account has been updated or created.  The full metadata details are sent:
```js
{
   "wotcha": {
      "userid": GUID,
      "user_metadata": METADATA
   }
}
```
#### USER_METADATA_DELETE

A user’s metadata has been deleted.  Only the metadataID is sent:
```js
{
   "wotcha": {
      "userid": GUID,
      "user_metadata_delete": {
         "metadataid": GUID
      }
   }
}
```
#### USER_TOKENS

The number of tokens the signed in user has, has been updated – usually after a spend action.
```js
{
   "wotcha": {
      "userid": GUID,
      "user_tokens": {
         "tokens": number
      }
   }
}
```
### CHANNELS

#### CHANNEL_NEW

A new Channel has been created.  The full details of the Channel are sent:
```js
{
   "wotcha": {
      "channel_new": CHANNEL
   }
}
```
#### CHANNEL_SET

Channel details have been changed (except for the name).  The full details of the Channel are sent:
```js
{
   "wotcha": {
      "channel_set": CHANNEL
   }
}
```
#### CHANNEL_RENAME

A Channel has been renamed.  The full details of the Channel are sent:
```js
{
   "wotcha": {
      "channel_rename": CHANNEL
   }
}
```
#### CHANNEL_DELETE

A Channel has been deleted.
```js
{
   "wotcha": {
      "channel_delete": { "channelid": GUID }
   }
}
```
#### CHANNEL_METADATA

A metadata value on the Channel has either been created or modified.
```js
{
   "wotcha": {
      "channel_metadata": {
         "channelid": GUID,
         "metadataid": GUID,
         "key": string,
         "value": string | number | boolean | array | object
      }
   }
}
```
#### CHANNEL_METADATA_DELETE

A metadata value on the Channel has been deleted.
```js
{
   "wotcha": {
      "channel_metadata_delete": {
         "channelid": GUID,
         "metadataid": GUID
      }
   }
}
```
### GEOBOTS

#### GEOBOT_NEW

A new Geobot has been created on the channel.  This is received on Channels that are being watched.
```js
{
   "wotcha": {
      "geobot_new": GEOBOT
   }
}
```
#### GEOBOT_SET

A Geobot has been modified.
```js
{
   "wotcha": {
      "geobot_set": GEOBOT
   }
}
```
#### GEOBOT_DELETE

A Geobot has been deleted.
```js
{
   "wotcha": {
      "geobot_delete": { "geobotid": GUID, "channelid": GUID }
   }
}
```
#### GEOBOT_AUTOMATIONS

Automations have been altered.
```js
{
   "wotcha": {
      "geobot_automations": {
         "geobotid": GUID,
         "automations": [
            AUTOMATION, AUTOMATION, ...
         ]
      }
   }
}
```
#### GEOBOT_METADATA

A metadata value on the Geobot has either been created or modified.
```js
{
   "wotcha": {
      "geobot_metadata": {
         "geobotid": GUID,
         "metadataid": GUID,
         "key": string,
         "value": string | number | boolean | array | object
      }
   }
}
```
#### GEOBOT_METADATA_DELETE

A metadata value on the Geobot has been deleted.
```js
{
   "wotcha": {
      "geobot_metadata_delete": {
         "geobotid": GUID,
         "metadataid": GUID
      }
   }
}
```
#### GEOBOT_STATE_CHANGE

An Automation on a Geobot has changed state.
```js
{
   "wotcha": {
      "geobot_state_change": {
         "geobotid": GUID,
         "automationid": GUID,
         "newstate": string
      }
   }
}
```
#### GEOBOT_STATUS

The status of the Automations on a Geobot are sent on request with the "status" command on the websocket.
```js
{
   "wotcha": {
      "geobot_status": {
         "geobotid": GUID,
         "states": [
            { GUID : string }, // GUID is the AutomationID, and string is the state
            { GUID : string }, 
            ...
         ]
      }
   }
}
```
#### TRIGGER

A Trigger event is received.  This will have come from an Action sending a Trigger event to all devices wotching.
```js
{
   "wotcha": {
      "trigger": {
         "geobotid": GUID,
         "event": string,
         "parameters": object
      }
   }
}
```
## SENDING

Sending a command is likewise straightforward.  The only communication that a device can make with a Geobot through the websocket, is through sending an event, with a set of parameters including the ID of the object to get the event, the name of the event, and a payload of parameters which can be used by the Geobot getting the event.  Presently, only Geobots can received events, however future versions may allow Companions to also receive and notice events.

### EVENT

To send an event notification to a Geobot:
```js
let command = {
   command : "event",
   sessionid : sessionid,
   parameters : {
      id : GUID,
      event : string,
      parameters : parameters_object
   }
};
websocket.send( JSON.stringify( command ) );
```
### STATUS

Sometimes, it is helpful to know the states of the Automations on a Geobot.  The Status command triggers the Geobot to send the states of all its Automations.
```js
let command = {
   command : "status",
   sessionid : sessionid,
   parameters : {
      id : GUID
   }
};
websocket.send( JSON.stringify( command ) );
```