# INTRODUCTION

The Imersia Mixed Reality (MR) server is a content, communication and gamification server designed for managing Mixed Reality experiences between multiple people via their mobile devices, wearables or AR headsets, giving an illusion of multiple digital characters (digital agents) in the real world around them.

To achieve this, it uses two forms of communication with mobile devices, websites and WebApps; a REST API using HTTP/1.1 or HTTP/2, and websockets. The former is useful for issuing commands and getting information from the MR Server, whereas the latter is best for direct communication during interactive sessions and communicating directly with digital agents.

The API is a traditional Query/Response system, whereas the websockets use a Listen/Interrupt style of communication.

# LICENSING

When using the API, all calls are tagged with your developerID. This is associated with your License Code and together validates the usage of the MRServer. The License code is entered into the mrserver.toml file and is tied to the server's IP address and name.

# FEEDBACK

Any bugs, inconsistencies, suggestions for improvements or praise can be sent to mrserver@imersia.com. A full bug tracking system will be set up soon.
