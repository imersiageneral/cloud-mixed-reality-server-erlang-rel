# API COMMANDS

[TOC]

In this section, the API commands are detailed. These are organised by the URI first, then by the method.

## /API/SESSIONS

When first opening communications with your Companion, you identify yourself with a username and password, and potentially a token from another authentication provider. Your companion returns a time-limited SessionID which has to be used in all subsequent communications. In the future, the need to enter a username and password may be superseded by more subtle means of identification.

Your companion has an existence regardless of whether you are logged in or not, so is able to perform tasks for you even when you are not online.

Each device a user connects obtains their own SessionID, and their companion amalgamates the user interactions and analytics accordingly both across devices and time.

SessionIDs timeout after 2 hours of inactivity. However, if a SessionID is used before the timeout, this extends the period.

### HEAD

Check existence of a specific SessionID for this user's email address.

#### HEADERS

- useremail : string - The user's email address to check.

- sessionid : GUID - The SessionID to check.

#### RESPONSES

- 200 OK

  - response-body : (empty)

- 401 Unauthorized

  - www-authenticate : location | developerid

- 404 Not Found

  - response-body : (empty)

### OPTIONS

Get the options available. No additional headers are required apart from the DeveloperID and Location.

#### RESPONSES

- 200 OK

  - allow : GET, HEAD, OPTIONS, DELETE

- 401 Unauthorized

  - www-authenticate : location | developerid

### GET

Get a new SessionID given the useremail and password - this implements a 'log in'. Optionally, this can also store a token from another service provider (eg Google or Facebook). The authentication parameters can be provided either as header variables, or using Basic Auth settings (in which case the Authorization header is set with the appropriate encrypted version of the email address and password).

#### HEADERS

- useremail : string - User's email address

- password : string - User's password

- token : string - A token from another authenticator usually using oAuth2.

- Authorization : JSON - Optional Basic Auth details.

#### RESPONSES

- 200 OK

  - response-body : {"sessionid": GUID}

- 401 Unauthorized

  - www-authenticate : location | developerid

- 404 Not Found

  - response-body : {"error": "password"} | {"error": "useremail"} | {"error": "database"}

### DELETE

Delete a given SessionID - this implements a 'log out'. All further calls to the API or Websocket with that SessionID become invalid and return an error. All wotches on a Channel or Geobot using this SessionID are removed.

#### HEADERS

- sessionid : GUID - SessionID to delete

- useremail : string - User's email address

#### RESPONSES

- 204 No Content

- 401 Unauthorized

  - www-authenticate : location | developerid

- 404 Not Found

  - response-body : {"error": "sessionid"} | {"error": "useremail"}

## /API/USER

### HEAD

Checks existence of this user for the given SessionID, and that this matches either the UserID or the Useremail. The UserID can be placed either in the headers, or as a parameter in the URL.

eg. <https://imersia.org/api/user/d65ed1b4-a505-11e8-b142-35fc1621a7fe>

#### HEADERS

- userid : GUID - User's ID

- useremail : string - User's email address

#### RESPONSES

- 200 OK

  - response-body : (empty)

- 401 Unauthorized

  - www-authenticate : location | developerid

- 404 Not Found

  - response-body : (empty)

### OPTIONS

Get the options available. No additional headers are required apart from the DeveloperID and Location.

#### RESPONSES

- 200 OK

  - allow : GET, POST, PUT, HEAD, OPTIONS, DELETE

- 401 Unauthorized

  - www-authenticate : location | developerid

### GET

Get a specified User's details. The UserID can be placed either in the headers, or as a parameter in the URL. eg <https://imersia.org/api/user/d65ed1b4-a505-11e8-b142-35fc1621a7fe>, or the query can send a useremail address in the headers.

#### HEADERS

- userid : GUID - User's ID, either in the headers or the query.

- useremail : string - One of useremail or userid is required.

#### RESPONSES

- 200 OK

  - response-body :
      ```javascript
      USER (with empty password)
      ```
- 401 Unauthorized

  - www-authenticate : location | developerid

- 404 Not Found

  - response-body : {"error": "password"} | {"error": "useremail"}

### POST

Create a new user. Body contains the new user's full details including the email address and desired password. Note that the error 'useremail' can signify either that one wasn't passed as a parameter, or that a user with the email address already exists.  Many of the errors that may be returned are related to the validity of the passcode.

#### HEADERS

- passcode: string - A valid passcode associated with the email address in the body.
- (also the usual, developerid and location, but sessionid is not required)

#### BODY

```javascript
USER
```

#### RESPONSES

- 200 OK response-body :

  - {"userid": GUID}

- 401 Unauthorized

  - www-authenticate : location | developerid

- 400 Bad Request

  - response-body : {"error": "useremail"} | {"error": "password"} | {"error": "firstname"} | {"error": "surname"} | {"error": "nickname"} | {"error": "password"} | {"error": "invalid" } | {"error": "numtries"} | {"error": "timeout"}

### PUT

Update an existing user. User must already exist. The body contains the user details.

#### HEADERS

- n/a (i.e. just the usual, developerid, location and sessionid)

#### BODY

```javascript
USER_DETAILS
```

#### RESPONSES

- 200 OK

  - response-body : {"userid": GUID}

- 401 Unauthorized

  - www-authenticate : location | developerid

- 400 Bad Request

  - response-body : {"error": "userid"} | {"error": "sessionid"}

### DELETE

Delete a user. The UserID has to match the logged in user with the SessionID. Note that deleting a user deletes all their Channels, Geobots, Metadata, Files, etc. There is NO UNDO...

#### HEADERS

- n/a (i.e. just the usual, developerid, location and sessionid)

#### RESPONSES

- 200 OK

  - response-body : {"userid": GUID}

- 401 Unauthorized

  - www-authenticate : location | developerid | sessionid | channelid

- 404 Not Found

  - response-body : {"error": "userid"}

## /API/USER/PASSWORD

### PUT

Change password for a specified user. All existing SessionIDs for this user become invalid after a password has been changed. To double check, the user's UserID is required along with the new password in the headers.

Alternatively, the password can be changed using a one-time-passcode (see the passcode API).  This is useful for implementing a 'forgot password' functionality.

Many of the errors that may be returned are related to the validity of the passcode.

#### HEADERS

Either

- userid : GUID - The userid that corresponds to the logged in user sessionid.
- password : string - The new password.
- (sessionid is also required)

Or

- useremail : string - The user's email address.
- passcode : string - A valid passcode that has been obtained via the passcode API.

#### RESPONSES

- 200 OK

  - response-body : {"userid": GUID}

- 401 Unauthorized

  - www-authenticate : location | developerid

- 404 Not Found

  - response-body : {"error": "userid"} | {"error": "sessionid"} | {"error": "password"} | {"error": "invalid" } | {"error": "numtries"} | {"error": "timeout"}

## /API/USER/TOKENS

### GET

Get the number of tokens this user has in their account. The UserID can be passed as a header or in the query, or is inferred from the SessionID. If the tokens system is not set in mrserver.toml, returns an error of "no_tokens".

If a startdate or enddate are passed in the headers, this command will also return a list of transactions between (and including) the dates. If only startdate is specified, all transactions from then until the current date are returned. If only the enddate is specified, all transactions a month preceding the enddate are returned.

#### HEADERS

- userid : GUID - The UserID for the user to show the list of channels for.

- startdate : string - Optional start date in YYYYMMDD format.

- enddate : string - Optional end date in YYYYMMDD format.

#### RESPONSES

- 200 OK

  - response-body : {"tokens": number, "transactions": object}

- 401 Unauthorized

  - www-authenticate : location | developerid

- 404 Not Found

  - response-body : {"error": "no_tokens"} | {"error": "sessionid"}

## /API/PASSCODES

### GET

Sends a one-time-passcode to the specified email address.  As the name suggests passcodes can only be used once - but they also expire after three minutes and can only be attempted three times before a new passcode must be issued.

#### HEADERS

- useremail : string - the email address to send the passcode to
- (location and developerid are required, but not sessionid)

#### RESPONSES

- 200 OK
  - response-body : {"ok", "sent"} - indicates that the passcode was sent by email.
- 404 Not Found
  - response-body may indicate the source of error, eg {"error, "smtp"} - meaning there is something wrong with the smtp settings in the settings file, or {"error", "database"} meaning there was some serious database error.  These require intervention by the system administrator.

## /API/CHANNELS

### HEAD

Checks existence of the given channel, regardless of whether it is hidden or not or whether the user defined by the sessionid should be able to see it or not. However, it gives no further information about the channel other than that one with this name exists. This is particularly useful for checking if a new channel name being suggested by a user has already been used on this server.

#### HEADERS

- channelid : GUID - The ID of the channel to delete.
- (Also the usual, developerid, location and sessionid)

#### RESPONSES

- 200 OK

  - response-body : (empty)

- 401 Unauthorized

  - www-authenticate : location | developerid

- 404 Not Found

### OPTIONS

Get the options available. No additional headers are required apart from the DeveloperID and Location.

#### RESPONSES

- 200 OK

  - allow : GET, POST, PUT, HEAD, OPTIONS, DELETE

- 401 Unauthorized

  - www-authenticate : location | developerid

### GET

Either get a list of Channels, or get the details about a Channel.

The GET command, when used without specifying a channelid, returns a list of channels, depending on the parameters specified, scenarios are:

- userid passed as a parameter - gets the channels for this userid. If this is the same as the logged in user as defined by the sessionid, then it gets this user's channels, showing the hidden ones if parameter showhidden is true (otherwise returns only the non-hidden channels if showhidden is false). If the userid is different to the logged in user, returns only the non hidden channels regardless of the showhidden parameter. Note that by default, if showhidden is not passed as a parameter when the userid matches the logged in user, all channels are returned for that user.

- userid not passed as a parameter, gets the channels for the logged in user as specified by the sessionid, with the same behaviour for showhidden as above.

If a ChannelID or name is specified, gets the specified channel's details, taking into account whether the channel is hidden or not. The name is supplied in the query (/api/channels/Channel Name).

#### HEADERS

- userid : GUID The UserID for the user to show the list of channels for.

- Showhidden : boolean Whether to show the hidden channels to the owner of the channels.

#### RESPONSES

Responses:

- 200 OK

  - response-body :

  - for a list of channels

    ```javascript
    [ CHANNEL, CHANNEL, ... ]
    ```

  - for a single channel's details

    ```javascript
    CHANNEL
    ```

- 401 Unauthorized

  - www-authenticate : location | developerid

- 404 Not Found

  - response-body : {"error": "channelid"} | {"error": "database"}

### POST

Create a new channel for the logged in user as specified by the sessionid. Note that the channel name has to be unique across all channels on this MR Server. The body contains the channel details. Any details omitted are filled in with default values.

#### HEADERS

- n/a (i.e. just the usual, developerid, location and sessionid)

#### BODY

```javascript
CHANNEL
```

#### RESPONSES

- 200 OK

  - response-body : { "channelid" : GUID }

- 400 Bad Request

  - response-body : { "error" : "name" }

- 401 Unauthorized

  - www-authenticate : location | developerid | sessionid

- 404 Not Found

  - response-body : { "error" : "database" }

### PUT

Update an existing channel for the logged in user as specified by the sessionid. Note that the channel name has to be unique across all channels on this server, so trying to change the channel name to one that already exists will return an error. The body contains the channel details to change - any details to remain the same should be just omitted.

#### HEADERS

- channelid : GUID - The ID of the channel to change.

#### BODY

```javascript
CHANNEL
```

#### RESPONSES

- 200 OK

  - response-body : { "channelid" : GUID }

- 400 Bad Request

  - response-body : { "error" : "name" }

- 401 Unauthorized

  - www-authenticate : location | developerid | sessionid | channelid

- 404 Not Found

  - response-body : { "error" : "channelid" } | { "error" : "database" }

### DELETE

Delete a channel for a specified user. Also deletes all the Geobots on the Channel, and all the metadata and files associated with the Channel and Geobots. THERE IS NO UNDO.

#### HEADERS

- channelid : GUID - The ID of the channel to delete.

#### RESPONSES

- 200 OK

  - response-body : { "channelid" : GUID }

- 401 Unauthorized

  - www-authenticate : location | developerid | sessionid | channelid

- 404 Not Found

  - response-body : { "error" : "database"}

## /API/GEOBOTS

### HEAD

Checks existence of the given Geobot, regardless of whether it is hidden or not or whether the user defined by the sessionid should be able to see it or not. However, it gives no further information about the Geobot other than that it exists.

#### HEADERS

- geobotid : GUID - The ID of the Geobot to update.
- (also the usual, developerid, location and sessionid)

#### RESPONSES

- 200 OK

  - response-body : (empty)

- 401 Unauthorized

  - www-authenticate : location | developerid

- 404 Not Found

### OPTIONS

Get the options available. No additional headers are required apart from the DeveloperID and Location.

#### HEADERS

- n/a (i.e. just the usual, developerid, location and sessionid)

#### RESPONSES

- 200 OK

  - allow : GET, POST, PUT, HEAD, OPTIONS, DELETE

- 401 Unauthorized

  - www-authenticate : location | developerid

### GET

Either gets the full list of Geobots on a Channel for the logged in user, or a list of Geobots around a given location, or the details of a specific Geobot.

- If the ChannelID is passed as a parameter - gets the Geobots for this ChannelID. If this Channel has the same owner as the logged in user as defined by the SessionID, then it gets the Geobots on the Channel, showing the hidden ones if parameter showhidden is true (otherwise returns only the non-hidden Geobots if showhidden is false). If the Channel's owner is different to the logged in user, returns only the non-hidden Geobots regardless of the showhidden parameter. Note that by default, if showhidden is not passed as a parameter when the Channel's owner matches the logged in user, all Geobots are returned for that Channel.

- Further, if a Geobot has a radius greater than 0, and the distance to the Geobot from the user is less than that radius distance, and taking into account whether it is hidden, then the Geobot will only be listed if showhidden is true and it is owned by the logged in user, otherwise it will not be listed.

#### HEADERS

- channelid : GUID - The ID of the Channel to get the Geobots on.

- location : GEOHASH - If getting a list by location, this is the point around which the search is done.

- radius : number - Distance around the location in meters to search.

- showhidden : boolean - Show the hidden Geobots if being called by the owner of the channel.

#### RESPONSES

- 200 OK

  - response-body : (single Geobot)

    ```javascript
    GEOBOT
    ```

  - or (list of Geobots)

    ```javascript
    [GEOBOT, GEOBOT, ... ]
    ```

- 401 Unauthorized

  - www-authenticate : location | developerid

- 404 Not Found

  - response-body : { "error" : "channelid" } | { "error" : "database" }

### POST

Create a new Geobot on the Channel specified for the user as defined by the SessionID. The body contains the Geobot details, with location either being specified in the body or used from the header.

#### HEADERS

- channelid : GUID - The ID of the Channel to create the Geobots on.

#### BODY

```javascript
GEOBOT
```

#### RESPONSES

- 200 OK

  - response-body : { "geobotid" : GUID }

- 401 Unauthorized

  - www-authenticate : location | developerid | sessionid | channelid

- 404 Not Found

  - response-body : { "error" : "database" }

### PUT

Update an existing Geobot for the user as defined by the SessionID and GeobotID. The body contains the Geobot details, with location either being specified in the body or used from the header. Any details not included are left unchanged.

#### HEADERS

- geobotid : GUID - The ID of the Geobot to update.

#### BODY

```javascript
GEOBOT
```

#### RESPONSES

- 200 OK

  - response-body : { "geobotid" : GUID }

- 401 Unauthorized

  - www-authenticate : location | developerid | sessionid | geobotid

- 404 Not Found

  - response-body : {"error": "database"}

### DELETE

Delete a Geobot. Note that you can only delete Geobots that you own. Deleting a Geobot removes all the metadata, automations and files as well. THERE IS NO UNDO.

#### HEADERS

- geobotid : GUID - The ID of the Geobot to delete.

#### RESPONSES

- 200 OK

  - response-body : { "geobotid" : GUID }

- 401 Unauthorized

  - www-authenticate : location | developerid | sessionid | geobotid

- 404 Not Found

## /API/GEOBOTS/LOG

Allows an App or WebApp to log an event to a Geobot without having to be connected via websocket. If the logged in user sending the event is not the Geobot's owner, then ContextIDs will be required to ensure the Geobot has capability 'log' enabled. Can also be called using http.

### OPTIONS

Get the options available. No additional headers are required apart from the DeveloperID and Location.

#### HEADERS

- n/a (i.e. just the usual, developerid, location and sessionid)

#### RESPONSES

- 200 OK

  - allow : POST, OPTIONS

- 401 Unauthorized

  - www-authenticate : location | developerid

### POST

Sends an event with paramteers to the designated Geobot.

#### HEADERS

- geobotid : GUID - The ID of the Geobot to send the event to.

- contextids : Array of ContextIDs - The relevant contextIDs to allow a user to log an event.

#### BODY

```javascript
{
   "event": STRING
   "parameters": OBJECT
}
```

#### RESPONSES

- 200 OK

  - response-body : { "ok" : "logged"}

- 401 Unauthorized

  - www-authenticate : location | developerid | sessionid | geobotid

- 404 Not Found

  - response-body : { "error" : "database" }

## /API/GEOBOTS/SEND

Allows an App or WebApp to send an event to a Geobot's Automations without having to be connected via websocket. If the logged in user sending the event is not the Geobot's owner, then ContextIDs will be required to ensure the Geobot has capability 'send' enabled. Can also be called using http.

### OPTIONS

Get the options available. No additional headers are required apart from the DeveloperID and Location.

#### HEADERS

- n/a (i.e. just the usual, developerid, location and sessionid)

#### RESPONSES

- 200 OK

  - allow : POST, OPTIONS

- 401 Unauthorized

  - www-authenticate : location | developerid

### POST

Sends an event with paramteers to the designated Geobot.

#### HEADERS

- geobotid : GUID The ID of the Geobot to send the event to.

- contextids: Array of ContextIDs The relevant contextIDs to allow a user to log an event.

#### BODY

```javascript
{
   "event": STRING
   "parameters": OBJECT
}
```

#### RESPONSES

- 200 OK

  - response-body : { "ok" : "logged"}

- 401 Unauthorized

  - www-authenticate : location | developerid | sessionid | geobotid

- 404 Not Found

  - response-body : { "error" : "database" }

## /API/GEOBOTS/AUTOMATIONS

Automations are the way that Geobots can be programmed. These API commands set up and update Automations. To send events and find out the current state of an Automation, use the websocket.

### HEAD

Same as /api/geobots HEAD command.

#### HEADERS

- n/a (i.e. just the usual, developerid, location and sessionid)

#### RESPONSES

- 200 OK

  - response-body : (empty)

- 401 Unauthorized

  - www-authenticate : location | developerid

- 404 Not Found

### OPTIONS

Get the options available.

#### HEADERS

- n/a (i.e. just the usual, developerid, location and sessionid)

#### RESPONSES

- 200 OK

  - allow : GET, POST, PUT, HEAD, OPTIONS, DELETE

- 401 Unauthorized

  - www-authenticate : location | developerid

### GET

Either gets a list of the Automations on the specified Geobot, or the details of the specified Automation if given an AutomationID.

#### HEADERS

- geobotid : GUID The ID of the Geobot to get the Automations on.

- automationid : GUID The ID of the Automation to get. If not present, gets a list of Automations.

#### RESPONSES

- 200 OK

  - response-body :

    ```javascript
    [AUTOMATION | AUTOMATION ...]
    ```

    or

    ```javascript
    AUTOMATION
    ```

### POST

Creates a new Automation on the given Geobot. When the Automation is created, it begins to run immediately, and sends the start event.

#### HEADERS

- geobotid : GUID The ID of the Geobot to add the Automation to.

#### BODY

AUTOMATION

#### RESPONSES

- 200 OK

  - response-body : { "automationid" : GUID }

- 401 Unauthorized

  - www-authenticate : location | developerid | sessionid | geobotid

- 404 Not Found

### PUT

Update an existing Automation on the given Geobot.

#### HEADERS

- geobotid : GUID The ID of the Geobot to update the Automation on.

- automationid : GUID The ID of the Automation to update.

#### BODY

```javascript
AUTOMATION
```

#### RESPONSES

- 200 OK

  - response-body : { "automationid" : GUID }

- 401 Unauthorized

  - www-authenticate : location | developerid | sessionid | geobotid

- 404 Not Found

### DELETE

Delete the specified Automation from the specified Geobot.

#### HEADERS

- geobotid : GUID The ID of the Geobot to delete the Automation from.

- automationid : GUID The ID of the Automation to delete.

#### RESPONSES

- 200 OK

  - response-body : { "automationid" : GUID }

- 401 Unauthorized

  - www-authenticate : location | developerid | sessionid | geobotid

- 404 Not Found

## /API/METADATA

Metadata can be applied to Companions, Channels and Geobots. The same commands are used for them all, however, the relevant ID and type must be specified in the headers as userid, channelid or geobotid respectively.

### HEAD

Checks existence of the metadata on the given entity, regardless of whether it is hidden or not or whether the user defined by the SessionID should be able to see it or not. However, it gives no further information about the entity other than that the metadata of that id exists.

#### HEADERS

- geobotid | channelid | userid : GUID The ID of the entity to find the metadata on.

- metadataid : GUID The ID of the metadata to check.

#### RESPONSES

- 200 OK

  - response-body : (empty)

- 401 Unauthorized

  - www-authenticate : location | developerid

- 404 Not Found

### OPTIONS

Get the options available.

#### HEADERS

- n/a (i.e. just the usual, developerid, location and sessionid)

#### RESPONSES

- 200 OK

  - allow : GET, POST, PUT, HEAD, OPTIONS, DELETE

- 401 Unauthorized

  - www-authenticate : location | developerid

### GET

Either get a list of metadata on this entity, or the specified metadata details if a MetadataID is given.

#### HEADERS

- geobotid | channelid | userid : GUID The ID of the entity to find the metadata on.

- metadataid : GUID The ID of the metadata to check. If not given, returns a list.

#### RESPONSES

- 200 OK

  - response-body :

    ```javascript
    METADATA
    ```

  - or

    ```javascript
    [ METADATA, METADATA, ... ]
    ```

- 401 Unauthorized

  - www-authenticate : location | developerid | userid

- 404 Not Found

  - response-body : { "error" : "userid"} | { "error" : "geobotid"} | { "error" : "channelid"} | {"error" : "database"}

### POST

Create a new metadata key/value.

#### HEADERS

- geobotid | channelid | userid : GUID The ID of the entity to create the metadata on.

#### BODY

```javascript
METADATA
```

#### RESPONSES

- 200 OK

  - response-body : { "metadataid" : GUID }

- 401 Unauthorized

  - www-authenticate : location | developerid | sessionid

- 404 Not Found

  - response-body : { "error" : "userid"} | { "error" : "geobotid"} | { "error" : "channelid"} | {"error" : "database"}

### PUT

Update an existing metadata entry for an entity. The metadata is contained in the body. If a metadata entry already exists with the same key, it is updated, thus the key can be changed as well as the value. If a metadataid is given, either in the headers or the body, both the metadata key and value are updated (if it exists, and if the user owns it).

#### HEADERS

- geobotid | channelid | userid : GUID The ID of the entity to update the metadata on.

- metadataid : GUID The ID of the metadata to update.

#### BODY

```javascript
METADATA
```

#### RESPONSES

- 200 OK

  - response-body : { "metadataid" : GUID }

- 401 Unauthorized

  - www-authenticate : location | developerid | sessionid

- 404 Not Found

  - response-body : { "error" : "metadataid"} | { "error" : "userid"} | { "error" : "geobotid"} | { "error" : "channelid"} | {"error" : "database"}

### DELETE

Delete a metadata entry. Note that you can only delete Metadata on entities that you own. The metadata is identified either by its metadataid or a key, as specified in the headers.

#### HEADERS

- geobotid | channelid | userid : GUID The ID of the entity to update the metadata on.

- metadataid : GUID The ID of the metadata to delete.

#### RESPONSES

- 200 OK

  - response-body : { "metadataid" : GUID }

- 401 Unauthorized

  - www-authenticate : location | developerid | sessionid | geobotid

- 404 Not Found

  - response-body : { "error" : "metadataid"} | { "error" : "userid"} | { "error" : "geobotid"} | { "error" : "channelid"} | {"error" : "database"}

## /API/FILES

Files can be added to Companions, Channels and Geobots. The same commands are used for them all, however, the relevant ID and type must be specified in the headers as userid, channelid or geobotid respectively.

### OPTIONS

Get the options available.

#### HEADERS

- n/a (i.e. just the usual, developerid, location and sessionid)

#### RESPONSES

- 200 OK

  - allow : GET, POST, OPTIONS, DELETE

- 401 Unauthorized

  - www-authenticate : location | developerid

### GET

Get a list of files for this entity. The entity is specified in the headers with either userid, channelid or geobotid. The filenames are returned as an unordered array of strings. Each filename is a complete relative URL, eg: "/files/d4c0cc4e-d7fb-11e8-ac76-e5a8e9015b21/Penguins_of_Madagascar.mp4".

#### HEADERS

- geobotid | channelid | userid : GUID The ID of the entity to get the list of files from.

#### RESPONSES

- 200 OK

  ```javascript
  {
    "fileurls": [ string, string, ... ]
  }
  ```

- 401 Unauthorized

  - www-authenticate : location | developerid

- 404 Not Found

  - response-body : { "error" : "userid"} | { "error" : "geobotid"} | { "error" : "channelid"} | {"error" : "database"}

### POST

Post a file or files to the given Entity. The files are sent as a multipart body. If a file is sent with a filename that already exists, it is overwritten.

#### BODY

Files are sent using the multipart protocol[[1]](applewebdata://C5AD47EC-387F-4EB9-A64C-0573836078DC#_ftn1). Most http APIs support this natively. The OK response returns the relative URLs of the new files.

#### HEADERS

- geobotid | channelid | userid : GUID The ID of the entity to post the file(s) to.

#### RESPONSES

- 200 OK

  ```javascript
  {
    "fileurls": [ string, string, ... ]
  }
  ```

- 401 Unauthorized

  - www-authenticate : location | developerid

- 404 Not Found

  - response-body : { "error" : "userid"} | { "error" : "geobotid"} | { "error" : "channelid"}

### DELETE

Delete a file from the given entity.

#### HEADERS

- geobotid | channelid | userid : GUID The ID of the entity to delete the file from.

- filename : string The name of the file to delete, without the relative path.

#### RESPONSES

- 200 OK

  ```javascript
  {
    "fileurl": string
  }
  ```

- 401 Unauthorized

  - www-authenticate : location | developerid

- 404 Not Found

  - response-body : { "error" : "userid"} | { "error" : "geobotid"} | { "error" : "channelid"}

## /API/ANALYTICS

### OPTIONS

Get the options available.

#### HEADERS

- n/a (i.e. just the usual, developerid, location and sessionid)

#### RESPONSES

- 200 OK

  - allow : GET, POST, OPTIONS

- 401 Unauthorized

  - www-authenticate : location | developerid

### GET

Get analytics for the given entity between the dates given, for the event given. If no event is specified, gets all the events between the dates. If no dates are given, gets analytics between the current date and a month ago. If only the startdate is given, gets events from that date until the present time. If only the enddate is given, gets events from a month ending at the enddate. Dates are taken to be at 0 GMT both in the parameters, and the data returned, so conversion to local timezone is required.

#### HEADERS

- geobotid : GUID Geobot to get Analytics from.

- event : string Optional event to query.

- startdate : string Optional start date in YYYYMMDD format.

- enddate : string Optional end date in YYYYMMDD format.

#### RESPONSES

- 200 OK

  ```javascript
  [
    ANALYTIC, ANALYTIC, ...
  ]
  ```

- 401 Unauthorized

  - www-authenticate : location | developerid

- 404 Not Found

  - response-body : { "error" : "geobotid" } | { "error" : "database" }

### POST

Post a new event to the Geobot, with optional parameters. All events are tagged with the current location and date/time. Note that parameters sent have to be valid JSON. If invalid JSON is sent, it will not be recorded, but no error is returned.

#### HEADERS

- geobotid : GUID Geobot to post event for analytics to.

#### BODY

```javascript
{
   "event" : string,
   "parameters" : array | object
}
```

#### RESPONSES

- 200 OK

  ```javascript
  {
    "analyticid" : GUID
  }
  ```

- 400 Bad Request

  - response-body : { "error" : "event" }

- 401 Unauthorized

  - www-authenticate : location | channelid | developerid

- 404 Not Found

  - response-body : { "error" : "geobotid" } | { "error" : "database" }

## /API/VARS

This command accesses the values stored in the mrserver.toml file, and some other variables related to this instance of the MRServer.

### GET

Gets a value from the mrserver.toml file. Useful, for example, in getting the default values for, say, the mapbox ID that is then used both in the portal and user experience.

The version of the currently running MR server can be obtained with category=mrserver and key=version. The value returned is of the form RELEASE.MAJOR.MINOR-YEAR.MONTH.DAY

#### HEADERS

- category: string The category (the ones in the square brackets in the ini file)

- key : string The key below the category for the value desired.

#### RESPONSES

- 200 OK

  - response-body : { "value" : VALUE}

- 401 Unauthorized

  - www-authenticate : location | developerid

- 404 Not Found

  - response-body : { "error" : "undefined"}

## /API/CONTEXT

Contexts can be applied to Channels and Geobots. The same commands are used for them all, however, the relevant ID and type must be specified in the headers as channelid or geobotid respectively.

### HEAD

Checks existence of the context on the given entity, regardless of whether it is hidden or not or whether the user defined by the SessionID should be able to see it or not. However, it gives no further information about the entity other than that the metadata of that id exists.

#### HEADERS

- geobotid | channelid : GUID The ID of the entity to find the context on.

- contextid : GUID The ID of the context to check.

#### RESPONSES

- 200 OK

  - response-body : (empty)

- 401 Unauthorized

  - www-authenticate : location | developerid

- 404 Not Found

### OPTIONS

Get the options available.

#### HEADERS

- n/a (i.e. just the usual, developerid, location and sessionid)

#### RESPONSES

- 200 OK

  - allow : GET, POST, PUT, HEAD, OPTIONS, DELETE

- 401 Unauthorized

  - www-authenticate : location | developerid

### GET

Either get a list of contexts on this entity, or the specified context details if a ContextID is given.

#### HEADERS

- geobotid | channelid : GUID The ID of the entity to find the context on.

- contextid : GUID The ID of the context to check. If not given, returns a list.

#### RESPONSES

- 200 OK

  - response-body :

    ```javascript
    CONTEXT
    ```

  - or

    ```javascript
    [ CONTEXT, CONTEXT, ... ]
    ```

- 401 Unauthorized

  - www-authenticate : location | developerid | userid

- 404 Not Found

  - response-body : { "error" : "geobotid"} | { "error" : "channelid"} | {"error" : "database"}

### POST

Create a new context.

#### HEADERS

- geobotid | channelid : GUID The ID of the entity to create the metadata on.

#### BODY

```javascript
CONTEXT
```

#### RESPONSES

- 200 OK

  - response-body : { "contextid" : GUID }

- 401 Unauthorized

  - www-authenticate : location | developerid | sessionid

- 404 Not Found

  - response-body : { "error" : "geobotid"} | { "error" : "channelid"} | {"error" : "database"}

### PUT

Update an existing context entry for an entity. The context is contained in the body.

#### HEADERS

- geobotid | channelid : GUID The ID of the entity to update the context on.

- contextid : GUID The ID of the context to update.

#### BODY

```javascript
CONTEXT
```

#### RESPONSES

- 200 OK

  - response-body : { "contextid" : GUID }

- 401 Unauthorized

  - www-authenticate : location | developerid | sessionid

- 404 Not Found

  - response-body : { "error" : "contextid"} | { "error" : "geobotid"} | { "error" : "channelid"} | {"error" : "database"}

### DELETE

Delete a context entry. Note that you can only delete Contexts on entities that you own.

#### HEADERS

- geobotid | channelid : GUID The ID of the entity to update the context on.

- contextid : GUID The ID of the context to delete.

#### RESPONSES

- 200 OK

  - response-body : { "contextid" : GUID }

- 401 Unauthorized

  - www-authenticate : location | developerid | sessionid | geobotid

- 404 Not Found

  - response-body : { "error" : "metadataid"} | { "error" : "geobotid"} | { "error" : "channelid"} | {"error" : "database"}

## /GEOJSON

Gets a GeoJSON formatted list of publicly viewable Channels or Geobots. Can also be called using http.

### OPTIONS

Get the options available.

#### HEADERS

- n/a (is a public function).

#### RESPONSES

- 200 OK

  - allow : GET, HEAD, OPTIONS

### GET

Get either a list of CHANNELS and GEOBOTS or a list of GEOBOTS on a channel.

If a channel name is specified in the URL (eg <https://imersia.org/geojson/AChannel>), gets the specified channel's details, and all the publicly viewable Geobots on that channel within radius of the location. If the radius is zero (or undefined), gets all the Geobots on that channel. Only returns the channel if it is itself publicly viewable.

If a channel name is not specified (ie <https://imersia.org/geojson>), gets a list of publicly viewable channels on this MRServer, and a list of publicly viewable geobots within the radius of the location given.  If radius is not specified or is zero, returns all publicly viewable geobots - be warned, this could be a very large array...

Note that location still has to be specified even if intending to get a channel's details or getting all geobots.

The Geobots and Channels returned are ordered alphabetically.

#### HEADERS

- location : GEOHASH The location (geohash) from which to look for geobots.

- radius : number The radius (meters) from which to look around the location.

#### RESPONSES

- 200 OK

  - Either a list of channels and geobots:

    ```javascript
    {
      "type" : "FeatureCollection",
      "channels" : [ CHANNEL, CHANNEL, … ],
      "features" : [ GEOBOT, GEOBOT, … ]
    }
    ```

  - Or a channel with list of geobots:

```javascript
CHANNEL_GEOJSON
```

- 401 Unauthorized

  - www-authenticate : location

## /MAPTILE

The MR Server can cache maptiles for all users of the server, which are loaded via the maptile API. When the GET command is called, it returns the appropriate maptile requested. if a URL is given in the parameters, it loads that tile from the URL, caches it, and passes back the image. If no URL is passed in, it simply passes the cached image if it exists, or a blank image otherwise.

### GET

Get a maptile image. The main parameters are in the URL:

- {folder name}/{zoom}/{x}/{y}

e.g.:

- <https://imersia.org/maptile/com.mapbox.light/6/58/40>

And the URL parameter contains

- url = url of the original map tile,

e.g.:

- url=<https://api.mapbox.com/styles/v1/mapbox/light-v9/tiles/512/6/58/40?access_token={mapbox> token}

#### HEADERS

- n/a (is a public function).

#### RESPONSES

- 200 OK

  - response-body : FILE (the image)

- 404 Not Found

  - response-body : empty

## /FILES

To load a file in a browser or other similar reader or device, a normal GET request is sent to the relative URL provided when listing files on an entity. No headers are required, and thus no checks are done on whether the file specified is on a hidden entity. However, the GET request must specify the exact file name, and cannot list all files. Can also be called using http.

Files are passed through the image processor and may be modified on-the-fly by additional query parameters, thus reducing the size of files being transmitted to mobile devices (if they are images). Processed images are always sent in PNG format, regardless of the original image type. Non-image files are passed through unchanged, with video and sound being able to be streamed.

Some entities may be hidden, in which case their details, including their files, are not publicly available.  Since normal GET commands as used in html oftentimes don't have the facility to pass additional headers, sessionid and  contextids may also be included in the request instead of in the headers.

### GET

Get a file as specified in the Query. Additional parameters can be used to modify image files on-the-fly, for example: /files/caad0a6e-5e7a-11e9-a091-e5a8f25ec2f6/explosion.png?size=thumb&shape=round

#### QUERY

- size : string thumb | small | medium | large
- shape : string round | circle | square
- sessionid : for accessing hidden files
- contextids : either an array of strings (contextids) or a single contextid

#### HEADERS

- sessionid : for accessing hidden files (optional)
- contextids : either an array of strings (contextids) or a single contextid (optional)

#### RESPONSES

- 200 OK

  - response-body : FILE (the file requested, if it exists)

- 404 Not Found

  - response-body : empty

## /API/ADMIN (ALPHA)

If activated, a user (the superadmin as designated in the mrserver.toml file) can issue commands to allow administration of an MR Server. These are all funnelled through a single API call, with the command specified in the parameters. The user has to be logged in as the superuser before admin commands can be issued.

This capability is still under development – more features to come...

### GET

Get details as an admin – the specifics of which are defined in the headers. Possible commands with parameters are:

- **stats**{} – return the number of users, Channels and Geobots on this MR Server

- **users**{} – return a list of users

- **channels**{"userid": GUID} – return a list of channels for the given user.

- **geobots**{"userid": GUID, "channelid": GUID} – return a list of geobots on the given channel.

- **metadata**{"id": GUID} – return a list of metadata for the given entity (user, channel or geobot).

- **analytics**{"userid": GUID, "geobotid": GUID, "startdate": DATE, "enddate": DATE, "event": string} – returns analytics details in the same format as the GET api/analytics command, but for a given user where DATE is in YYYYMMDD format.

- **automations**{"geobotid": GUID} – return the automation on a geobot in the same format as the GET api/geobots/automations command.

#### HEADERS

- command: string

- parameters: object

#### RESPONSES

- 200 OK

  - response-body : object – related to the admin command sent.

- 415 Unauthorized

  - response-body : empty

### POST

Make a few changes to entities on the MRServer.

- **lock** {"geobotid": GUID} – sets the hidden field of a Geobot to true.

- **unlock** {"geobotid": GUID} – sets the hidden field of a Geobot to false.

- **lock** {"channelid": GUID} – sets the hidden field of a Channel to true.

- **unlock** {"channelid": GUID} – sets the hidden field of a Channel to false.

#### HEADERS

- command: string

- parameters: object

#### RESPONSES

- 200 OK

  - response-body : {ok: locked} or {ok: unlocked} as appropriate.

- 415 Unauthorized

  - response-body : empty

--------------------------------------------------------------------------------

[[1]](applewebdata://C5AD47EC-387F-4EB9-A64C-0573836078DC#_ftnref1)<https://en.wikipedia.org/wiki/MIME#Multipart_messages>
