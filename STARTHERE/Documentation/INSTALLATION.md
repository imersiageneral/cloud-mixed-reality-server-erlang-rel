# INSTALLATION

[TOC]

The Imersia MR Server is still an 'invitation only' technology, and not available yet to the general public. Nevertheless, it has been designed based on knowledge gained from the previous versions of the Imersia Mixed Reality solutions, with many refinements and improvements. It is well tested and built according to best practices in user experience design, security and scalability.

We are keen to make the MR Server available to early adopters in both commercial and research contexts.

## DOWNLOADING THE LATEST VERSION

The latest version of the Imersia MR Server runtime can by cloned from the Imersia MR Server GIT repository.

```bash
git clone https://bitbucket.org/imersiageneral/cloud-mixed-reality-server-erlang-rel.git
```

## INSTALLING TO A CONTAINER OR A VM

The Imersia MR Server can be installed on many types of operating system: Linux, BSD, MacOS or Windows – essentially anything that can run Erlang and C. Further, it can be installed to run directly on the computer, or inside a container (such as Docker). Likewise, the computer can itself be a 'bare metal' machine, or a virtual machine, and will run happily on Single Board Computers to high-end games machines or cloud appliances. Of course, the performance will reflect the device on which it is running.

The most reliable and consistent method is to run an MR Server inside a Docker container. The following instructions are written assuming that is the chosen method.

The docker container must be created with ports 80 and 443 open to allow API, web and websocket access. In the downloaded MR Server files, the folder "STARTHERE" contains scripts and instructions for getting started.

## INSTALLING THE PREREQUISITES

Once Docker is installed on your system (for testing, the community edition is quite sufficient[[1]](applewebdata://7F081FE5-7888-48E3-845A-921FC8786E9B#_ftn1)), execute the docker creation script inside the STARTHERE folder as below. This reads the Dockerfile and sets up the container with the prerequisites of ImageMagick, GIT, openSSL and MongoDB, and installs the latest version of the MR Server to the correct place on the disk. If not using MongoDB (for example on a Raspberry PI or other single board computer), comment out the appropriate section in the Dockerfile to avoid installing MongoDB, and make sure the database implementation is set to mnesia in the mrserver.toml file.

```bash
cd STARTHERE
docker build -t mrserver .
```

The container can then be started in the usual fashion

```bash
docker run -p 80:80 -p 443:443 -i -t mrserver
```

Alternatively, the MRServer can be found on [docker hub](https://hub.docker.com/r/imersia/mrserver).

## CREATING THE SSL KEYS

In order for the API, web and websockets to operate correctly using SSL, especially inside a web browser, obtain the appropriate SSL security certificate and key from your host provider, and put these into the ssl directory, renamed as mrserver.crt and mrserver.key respectively. If an intermediate certificate is created, this should be named intermediate.crt, otherwise duplicate mrserver.crt and name that intermediate.crt (so mrserver.crt and intermediate.crt can be the same file). If running on a local machine (ie localhost), self generated keys[[2]](applewebdata://7F081FE5-7888-48E3-845A-921FC8786E9B#_ftn2)can be used, but these won't work for MR Servers made available on the Internet due to restrictions imposed by web browsers.

## OBTAINING A LICENSE

In order to fully utilise the Imersia MRServer, you need to acquire a license. Presently, only a Community License is available, but if you are looking for a Commercial License, please contact us and we can discuss terms and conditions. Licensing is a two-stage process:

1) Go to [the Imersia licensor](https://portal.imersia.net) to request a license. You will need the domain name of the server, the names of the application(s) you want to cover with this license, and contact details. Application names should be in reverse DNS form, for example – portal.imersia.com. You can have any number of applications under one license.

2) A license code is generated that you then insert into the mrserver.toml file. The license is checked each time the MRServer is started, and periodically throughout each day.

Each application you develop that uses the MRServer API will need to send a valid application name along with every API call using the developerID in the headers. It is advantageous, but not a requirement, to use a different developerID for each application, website, webapp, etc. This would mean you can control which Applications are active or inactive within your own circle of users.

## SETTING THE PARAMETERS IN THE MRSERVER.TOML FILE

The mrserver.toml files contains parameters that allow you to refine the operation of the MR Server. The layout of the file uses the TOML standard[[3]](applewebdata://7F081FE5-7888-48E3-845A-921FC8786E9B#_ftn3)of section headers in square brackets and property=value lines within each section. Comments are preceded by a hash (#).

The MR Server INI file has the following sections, properties and values:

Key              | Value
---------------- | -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
**[license]**    | The MRServer license code as obtained from <https://portal.imersia.net>.
code             | = "_license code_"
**[database]**   | Parameters for the database implementation.
implementation   | = "*mongo*" \| "*mnesia*"
**[mnesia]**     | Section to hold mnesia database parameters.
dir              | = "_absolute directory location for mnesia to use_" - For example: dir="/MRServer/database"
**[authentication]**    | Detrails about the authentication process.
login   | = "_password_" \| "_passcode_" \| "_twofactor_" - This defines how the session GET API function behaves - password requires only useremail and password, passcode requires useremail and a one-time-passcode, and twofactor requires both password and passcode along with the useremail.
**[billing]**    | Details about billing.
implementation   | = "_billingfox_"
billingfox       | = "_billingfox API key_" - The billingfox API Key as created at www.billingfox.com
[file]           | This section contains parameters for the file storage implementation.
implementation   | = "_local_" - Currently just saves files to the local file system. 
**[maptoken]**   | Section to hold various map API tokens.
mapbox           | = "_mapbox API token_" - Currently the only map token required. Other map tokens can be placed here, for example google, tomtom or bing. These can be queried via the API to be used in web pages. For example, just set tomtom = _tomtom API token_ (no quote marks or brackets) for that value to be available.The Mapbox API is used in the standard portal implementation. If this parameter is not set, the map will be blank both in 2D and 3D modes.
**[superadmin]** | Section to hold information about the super admin (see admin commands).
account          | = "_email address of super admin account_" - For example: account="fred@thiscomputer.com"
**[smtp]**       | Section to hold information about the smtp (email) settings.
relay            | = "_address of SMTP server_" - For example: account="smtp.gmail.com"
username         | = "_SMTP account username_" - For example: username="sender@emailserver.com"
password         | = "_SMTP account password_" - For example: password="SMTPPASS"
from             | = "_Information to see in the sender details_" - for example "Do Not Reply <fred@dagg.com>" 
ssl | = _true \| false_ - Whether to use ssl or not.For example: ssl=true 
**[mrserver]** | Section with various other miscellaneous parameters. 
debug | = _true \| false_ - Determines the level of logging – debug=true gives more verbose logging, whereas debug=false logs only critical errors. When debug is turned on, the MR Server console will show debugging information.
name | = "_MR Server name_" - For example, mrserver@mrserver. This is used to identify and group MR Servers within a cluster. 
ssldir | = "_absolute directory location for the SSL files_" - Required for the correct operation of the secure API, webpages and websockets. Directory must contain the files mrserver.crt, mrserver.key and intermediate.crt (see ssl instructions). 
url | = "_base URL for this MR Server_" - For example url="imersia.org" or url="localhost".
allowhttp | = _true \| false_ - Determines whether the MR Server will deliver content found in the http folder or only the secure web content in the https folder. 
httpfolder | = "_name of the http folder_" - For example httpfolder="http". This is relative to the main MR Server folder. On installation, contains a simple default web app for listing and selecting Channels and Geobots. 
httpsfolder | = "_name of the secure http folder_" - For example httpsfolder="https". This is relative to the main MR Server folder. On installation, contains the standard MR Server portal.
httpdefault | = "_name of default http folder_" 
httpsdefault | = "_name of default https folder_"

## STARTING AND STOPPING THE MR SERVER

Once everything is set up, the MR Server can be set in operation. This should happen automatically when the container starts, however, sometimes it is desirable to run the MR Server in console mode. The following command starts the MR Server in console mode (assuming it has been installed in the default location):

```bash
/MRServer/mrserver/bin/mrserver console
```

To start the MR Server in normal mode, the following command is used:

```bash
/MRServer/mrserver/bin/mrserver start
```

To start the MongoDB server, the following command usually works, but check the documentation for your particular installation of MongoDB. If using the Erlang Mnesia database, it is started automatically within the MR Server.

```bash
/usr/bin/mongod --bind_ip_all
```

To stop an MRServer; issue the following command (the heart process is a daemon that restarts the MRServer under the unlikely situation that the entire erlang subsystem has crashed up to and including the main supervisory node):

```bash
killall heart
```

If you are running the MRServer outside of a Docker container, you will likely need to prepend the above commands with 'sudo' as super-user privileges are currently required. Except on more restricted systems such as Single Board Computers or VMs supplied by a cloud provider, it is therefore recommended to use a Docker container.

## INSTALLING ON RASPBERRY PI

The process of installation for Raspberry PI is exactly the same as any other machine or VM, however, use the version of the MRServer in the folder mrserver-zip/raspberry_pi - this is compiled for the ARM processor. Further, the mnesia database is recommended, and it is best to not install inside a docker container for performance reasons.

--------------------------------------------------------------------------------

[[1]](applewebdata://7F081FE5-7888-48E3-845A-921FC8786E9B#_ftnref1)<https://docs.docker.com/install/>

[[2]](applewebdata://7F081FE5-7888-48E3-845A-921FC8786E9B#_ftnref2)<https://www.dynacont.net/documentation/linux/openssl/>

[[3]](applewebdata://7F081FE5-7888-48E3-845A-921FC8786E9B#_ftnref3)<https://en.wikipedia.org/wiki/TOML>