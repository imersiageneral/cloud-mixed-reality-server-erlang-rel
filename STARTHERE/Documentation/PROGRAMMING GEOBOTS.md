# PROGRAMMING GEOBOTS

[TOC]

## AUTOMATIONS

Geobots are programmed using 'Automations', also sometimes called 'Finite State Machines'[[1]](applewebdata://EE26DB65-6CE8-49CA-BB7E-712B04272688#_ftn1). These are a way of describing the different 'states' that something can be in, and what happens when they change from one state to another. Each Geobot is Turing complete[[2]](applewebdata://EE26DB65-6CE8-49CA-BB7E-712B04272688#_ftn2).

![Figure2](./Figure2.png)

Figure 2: A finite state machine (or Automation)

The above example is a light switch. This Automation can either be in the states 'init', 'on' or 'off', and you can either 'turnon' or 'turnoff' the switch, depending on what state it currently is in. These are 'Transitions'. Note that all Automations start in the 'init' state. When you 'turnon' the switch, that in turn signals the bulb to 'gobright', and likewise, when you 'turnoff' the switch, the bulb is signalled to 'godark'. These are Actions. The bulb Automation might be on the same or a different Geobot. Geobots can have any number of Automations, and each Automation can have any number of Transitions. Each Transition can have any number of Actions. There are many different types of Action as detailed below.

### DEFAULTS

When a Geobot's Automation is first set, or when a Geobot is reset, it starts in the 'init' state. Immediately, then a 'start' event is issued, which can be caught in a Transition to determine the next state directly after initialisation, with appropriate Actions related to the process of initialisation.

### LOGFILE.TXT

If an Action on an Automation gives an error, a message is written to the file 'logfile.txt' on the Geobot.  Further, if you want to see the debug messages as well as the error messages, set a metadata with the key "debug" to the value "true".  Note that with debugging on, the logfile can get quite large, so best to turn it off when no longer required.

### METADATA

All Geobots have metadata (ie variables) which can contain numbers, strings, boolean (_true_ or _false_), a JSON structure an expression or the error notifier _err_. These can be operated on in the actions of the transitions. A metadata which contains an expression is evaluated at the time it is used. Further, a JSON structure can itself contain an expression (as a string) which is also evaluated when referred to.

#### STRINGS OR VARIABLES

Sometimes, it can be unclear whether a string inside a JSON structure is intended as a string or a reference to a metadata value. For example, "hello + world" could either mean add the strings 'hello' and 'world' together to produce 'helloworld' or add the values of the metadata referenced by hello and world. If a string is intended, then enclosing it in single quotes will ensure it remains a string and is not evaluated as a metadata name, or similarly, adding a single ! to the end of a metadata name will ensure it is interpreted as such. If a metadata value is referenced that does not yet exist, it is created and its value set to 0.

So, 'hello' + 'world' will result in 'helloworld', and hello! + world! will result in the sum of whatever hello and world contain. Likewise, 'hello' + world! will add the value of metadata world to the string 'hello' to produce the result.

#### CONVERTING BETWEEN TYPES

Converting between the basic metadata types of string, numbers, booleans (true or false) and err occurs implicitly with the following logic: booleans to numbers: true = 1, false = 0 and err = -1\. Numbers to boolans: err = false, >=1 = true, < 1 = false. Where a string (or reference to a string) is used in a function or expression that needs a number or boolean, this is converted to number or boolean as appropriate. So, '1' + 2 would result in 3\. However, strings are given precedence over conversion, so '1' + '2' would result in the string '12'.

#### EXAMPLES

- A basic metadata value could be: 3 or 'hello world' or true. Quotes are not needed around strings or booleans in this context.

- A simple JSON metadata value could be: {"animals" : ["dog", "cat", "horse"]}.

- A basic metadata value containing an expression could be: value1 + value2 where value1 and value2 are names of other metadata fields or parameters passed in with the event. Note that if the variables value1 or value2 don't exist, they are assumed to be strings, so the result would be value1value2\. See section above about forcing a reference to a string or variable.

- A JSON metadata value containing a reference to another metadata value could be: {"sum" : "value1 + value2"}. To ensure referenced metadata values (and not strings), use !, for example {"sum" : "value1! + value2!"} will ensure the metadata values in value1 and value2 are used, and these are not interpreted as strings. Note that value1 or value2 could themselves be expressions.

### JSON PATHS

#### GETTING VALUES

Within expressions; numbers, booleans and strings can be references-by-name to another metadata field, or a parameter passed in along with the event. Further, these fields can themselves refer to other metadata or parameters within the path. To get access inside a JSON structure, a JSON path can be used. This constitutes a combination of strings and numbers separated by colons with the first element referring to the metadata key. So, if there was a metadata with the key 'types' with the value {"animals" : ["dog", "cat", "horse"]}, then the JSON path types:animals:1 would equal "dog" and types:animals would equal ["dog", "cat", "horse"].

If a JSON path does not correctly identify within the JSON structure, then err is returned. So, types:animals:4 would return err in the example above.

Array indices start at 1.

A section of a JSON path can itself be an expression. To denote this, enclose that section in square brackets. For example types:animals:[count] would get the value of count (which in itself might be the result of an expression) and then uses it as an index into the array.

Most of the parameters to the commands below can be JSON paths or expressions, for example, the event to send, or the geobotid to send a message to.

#### SETTING VALUES

JSON paths can also be used when setting metadata, so with the set command below – if the key is given as types:animals:3 and the value is 'cow', then the resulting metadata structure of types would end up being {"animals" : ["dog", "cat", "cow"]}. If the JSON path is invalid, the appropriate section as defined by the value passed in, is added to the JSON structure. For example, with the key as types:animals:4 and the value of 'cow', the result would be {"animals" : ["dog", "cat", "horse", "cow"]}. Any array reference greater than the size of the array results in the value being added to the end of array and any array reference less than or equal to zero adds it to the start.

To add a new field to a JSON structure, simply refer to it in the JSON path. So, setting types:plants to the value ["grass", "oak", "thistle"] with the original JSON structure above would result in {"animals" : ["dog", "cat", "horse"], "plants" : ["grass", "oak", "thistle"]}. This is a convenient way to create JSON structures.

JSON paths used when setting metadata can also contain expressions, for example setting types:animals:[count] to 'cow' would result in the array item at position count being set, or it being added to the array if count is larger than the array length.

### EXPRESSIONS

The value parameter of a Metadata and the parameters to most actions can be an expression with the following symbols and functions. All functions have parameters in brackets, eg sin ( 1.57 ). The expression is stored as a string. Numbers and boolean values can be references to other metadata or the passed-in values from the user generated event, expressions, or JSON paths.

Expression                 | Description
-------------------------- | ------------------------------------------------------------------------------------------------------------------------
123.45e67                  | Standard format numbers with or without exponent, and with or without negative (-)
true false                 | Boolean values
+ - * / %                  | Standard numerical operations. + also works to combine strings and arrays. % is for modulus.
( )                        | Brackets for setting precedence of operations
> < >= <= == !=            | Comparison operators – produces either true or false
&& \|\| !                       | Logical operators, and, or and not.                                                                                    
if(cond, a, b) | Evaluates condition cond, and if true returns value of expression a, or if false, value of expression b. 
cos(n) sin(n) tan(n)       | Standard trigonometric functions (angle parameter is in radians)
acos(n) asin(n) atan(n)    | Standard inverse trigonometric functions
atan2(n, n)                | Arc tangent, using signs to determine quadrants[[3]](applewebdata://EE26DB65-6CE8-49CA-BB7E-712B04272688#_ftn3)
cosh(n) sinh(n) tanh(n)    | Hyperbolic functions
acosh(n) asinh(n) atanh(n) | Inverse hyperbolic functions
ceil(n) floor(n)           | Rounding functions to round up or down respectively
round(n) | Round n to integer. 
log(n) log2(n) log10(n)    | Log functions, base e, 2 and 10 respectively
exp(n)                     | Exponential
pow(n, n)                  | Raise parameter 1 to the power of parameter 2
sqrt(n)                    | Square root
random() | Random number between 0.0 and 1.0. 
pi()                       | The value of p – takes no parameters, but requires the brackets
fmod(n, n)                 | Remainder of the floating point division (modulus). Same as %.
metadata_key               | Any metadata key can be used in the expression and refers to metadata on this Geobot. A metadata_key can be a JSON path.
geobot_detail              | A geobot's core string, numerical or boolean detail can be used in the expression.
length(expr)               | The length of an array or string, or a JSON path that refers to an array or string.

## ACTIONS

### SET

Set a metadata or detail value of a Geobot.

#### PARAMETERS

- key – the name of the Metadata or Geobot detail value to set. Geobot detail values include: name, description, class, imageurl, latitude, longitude, geohash, radius, and hidden. Note that a key can be a JSON path, as described above.

- value – either a valid JSON-defined value, or an expression, including JSON paths.

#### EXAMPLE

```javascript
{
   command: "set",
   parameters: {
      key: "counter",
      value: "counter + 1"
   }
}
```

### TEST

Evaluate an expression, and send an event depending on the outcome.

#### PARAMETERS

- test – An expression that evaluates to true or false.

- true_event – an event (string).

- false_event – an event (string).

- geobotid – optional geobotid to send event to. If not included, sends event to this Geobot.

#### EXAMPLE

```javascript
{
   command: "test",
   parameters: {
      test: "counter >= 10",
      true_event: "stop",
      false_event: "count",
      geobotid: "48ca7ff2-c2d7-11e8-9167-e7e1422dc19a"
   }
}
```

### SEND

Send an event to this or another Geobot, with or without a delay. If the URL is included, will send the event to a Geobot on a remote MRServer, but only if the Geobots are entangled first (see entangle command).

#### PARAMETERS

- event – an event.

- url – a remote MRServer's domain name or IP address (without the https://). No URL implies the Geobot is not remote.

- parameters – a JSON object.

- delay – period of time to wait (seconds) before sending the event.

- geobotid – optional geobotid to send event to. If not included, sends event to this Geobot.

#### LOCAL EXAMPLE

```javascript
{
   command: "send",
   parameters: {
      event: "jump",
      parameters: {
         height: 20
      },
      delay: 3,
      geobotid: "48ca7ff2-c2d7-11e8-9167-e7e1422dc19a"
   }
}
```

#### REMOTE EXAMPLE

```javascript
{
   command: "send",
   parameters: {
      event: "jump",
      parameters: {
         height: 20
      },
      delay: 3,
      geobotid: "9d307ef0-a68b-11e9-af1c-01fbdae3b56a",
      url: "mrserver.co.nz"
   }
}
```

### BROADCAST

Send an event to Geobots on a channel, within the given radius of this Geobot.

#### PARAMETERS

- event – the event to send.

- channelid – the Channel to find the Geobots on – blank sends the event to the same Channel as the Geobot.

- radius – distance around this Geobot to send event to (meters).

- delay – period of time to wait (seconds) before sending the event.

- parameters – a JSON object.

#### EXAMPLE

```javascript
{
   command: "broadcast",
   parameters: {
      event: "jump",
      parameters: {
         height: 20
      },
      delay: 3,
      radius: 100,
      channelid: "2bd0fc32-c2d7-11e8-b422-e7e15feea00e"
   }
}
```

### EMAIL

Send an email to the specified recipients. For this to work, the smtp settings in the mrserver.toml file have to be set.

#### PARAMETERS

- recipients – a comma separated list of email addresses. Can be formatted as a JSON list of strings, or as a single string of email addresses separated by commas. Can also be just one email address. Each recipient receives their own email message, without seeing the other recipients listed. An email address can either include the name and email: "name \<email address>", eg "Fred Dagg \<fred@dagg.com>" or just an email address.

- subject – a subject string for the email.

- message – a message string for the email. Can include html.

#### EXAMPLE

```javascript
{
   command: "email",
   parameters: {
      recipients: "fred@dagg.com, bruce@bailiss.com",
      subject: "hello world",
      message: "<b>This</b> is a <i>message</i> to the world."
   }
}
```

### TRIGGER

Trigger a synchronized event across all devices wotching this Geobot. This can be used to start animations, trigger sounds, etc.

#### PARAMETERS

- event – the trigger event to send.
- parameters – JSON object.

#### EXAMPLE

```javascript
{
   command: "trigger",
   parameters: {
      event: "animate",
      parameters: {
        "speed": 3
      }
   }
}
```

### SPEND

Spend a user's tokens. To activate this, set a Geobot to call the spend action when an event is received. This can then be activated from within the App or WebApp by sending the corresponding event to that Geobot. The user whose account is debited is defined as the logged-in user who sent the event to the Geobot which triggered this action to be executed. For example, this might be set on a Geobot that is hidden, but available for admin purposes through its ID, and triggered every time a user scans an AR Marker.

#### PARAMETERS

- tokens – the number of tokens to spend.

- description – a short description that describes this purpose, which will be attached to the record of the spend.

#### EXAMPLE

```javascript
{
   command: "spend",
   parameters: {
      tokens: 1,
      description: "ARVideo scan"
   }
}
```

### LOG

Log an event to the analytics database for this Geobot. Each event is tagged with the geobotid, location of the event and date/time of the event. The location is stored as an 8 character geohash, and the time is stored rounded down to the current hour. This allows more efficient storage of analytics. If a more refined location or time is required, these can be set through the log command parameters. Note that logging regular events that occur quite often can create a large database.

#### PARAMETERS

- event – the trigger event to send.

- parameters – valid JSON related to the event.

#### EXAMPLE

```javascript
{
   command: "log",
   parameters: {
      event: "sensor_read",
      parameters: {
         temperature: 20,
         windspeed: 3
      }
   }
}
```

### ENTANGLE

The process of entanglement between a local and remote Geobot is managed through the entangle action. Once a local Geobot is entangled with a remote one, all triggers on the remote Geobot are seen as events on the local Geobot, and can hence be used in Transitions.

Entanglement requires login access to the remote server, therefore, the useremail and password for the user who owns the Geobot have to be included.

#### PARAMETERS

- url – the remote server's domain name or IP address.

- geobotid – the ID of the Geobot on the remote server.

- developerid – a valid developerid for the remote server as defined by the license.

- useremail – the email address of the user who owns the Geobot (or at least has a login on the remote server if the Geobot is publicly viewable).

- password – the login password.

#### EXAMPLE

```javascript
{
   command: "entangle",
   parameters: {
      url: "mrserver.co.nz",
      geobotid: "9d307ef0-a68b-11e9-af1c-01fbdae3b56a",
      developerid: "com.imersia.mrserver",
      useremail: "fred@dagg.com",
      password: "gumboots"
   }
}
```

### DISENTANGLE

Previously entangled Geobots can be disentangled by the initiating Geobot.

#### PARAMETERS

- url – the remote server's domain name or IP address.

- geobotid – the ID of the Geobot on the remote server.

- developerid – a valid developerid for the remote server as defined by the license.

#### EXAMPLE

```javascript
{
   command: "disentangle",
   parameters: {
      url: "mrserver.co.nz",
      geobotid: "9d307ef0-a68b-11e9-af1c-01fbdae3b56a",
      developerid: "com.imersia.mrserver",
   }
}
```

### LOAD

Load an Automation from a Metadata value on either a Geobot or Channel.

#### PARAMETERS

- name – the name of the Automation, and the Metadata key.
- geobotid – the ID of the Geobot to load from.
- channelid – The ID of the Channel to load from - one of either GeobotID or ChannelID should be present - if neither are present, the Automation is loaded from the current Geobot's metadata.
- contextids - An array of ContextIDs (or a single ContextID) to give read access to the Channel or Geobot being read from.

#### EXAMPLE

```javascript
{
   command: "load",
   parameters: {
      name: "Test Automation",
      geobotid: "9d307ef0-a68b-11e9-af1c-01fbdae3b56a",
      contextids: ["9c25c280-cf95-11e9-a647-35fc096e8efb", "7bbfb604-cf95-11e9-b634-35fc78b0a215"]
   }
}
```

where the value of Metadata 'Test Automation' might be:

```json
{
  "commands": [
    "jump"
  ],
  "description": "",
  "transitions": [
    {
      "actions": [
      ],
      "event": "start",
      "newstate": "idle",
      "state": "init"
    },
    {
      "actions": [
        {
          "command": "trigger",
          "parameters": {
            "event": "jump",
            "parameters": {
            }
          }
        }
      ],
      "event": "jump",
      "newstate": "idle",
      "state": "idle"
    }
  ]
}
```

### SAVE

Save an Automation to a Metadata value on either a Geobot or Channel.

#### PARAMETERS

- name – the name of the Automation, and the Metadata key to save to.  This has to the name of an existing Automation on the Geobot.
- geobotid – the ID of the Geobot to save to.
- channelid – The ID of the Channel to save to - one of either GeobotID or ChannelID should be present - if neither are present, the Automation is saved to the current Geobot's metadata.
- contextids - An array of ContextIDs (or a single ContextID) to give edit access to the Channel or Geobot being written to.

#### EXAMPLE

```javascript
{
   command: "save",
   parameters: {
      name: "Test Automation",
      geobotid: "9d307ef0-a68b-11e9-af1c-01fbdae3b56a",
      contextids: "9c25c280-cf95-11e9-a647-35fc096e8efb"
   }
}
```

## GEOBOT PROGRAMMING EXAMPLE

### A LIGHTSWITCH

A simple light switch that can trigger a real light to turn on and off.

```javascript
{
  "automationid": "48a51d14-d819-11e8-b210-e5a884780a45",
  "name": "switch",
  "description": "A simple lightswitch",
  "commands": [
    "turnon",
    "turnoff"
  ],
  "transitions": [
    {
      "state": "init",
      "event": "start",
      "newstate": "off",
      "actions": [
      ]
    },
    {
      "state": "off",
      "event": "turnon",
      "newstate": "on",
      "actions": [
        {
          "command": "trigger",
          "parameters": {
            "event": "light_on"
          }
        }
      ]
    },
    {
      "state": "on",
      "event": "turnoff",
      "newstate": "off",
      "actions": [
        {
          "command": "trigger",
          "parameters": {
            "event": "light_off"
          }
        }
      ]
    }
  ]
}
```

--------------------------------------------------------------------------------

[[1]](applewebdata://EE26DB65-6CE8-49CA-BB7E-712B04272688#_ftnref1)<https://en.wikipedia.org/wiki/Finite-state_machine>

[[2]](applewebdata://EE26DB65-6CE8-49CA-BB7E-712B04272688#_ftnref2)<https://en.wikipedia.org/wiki/Turing_completeness>

[[3]](applewebdata://EE26DB65-6CE8-49CA-BB7E-712B04272688#_ftnref3)<https://en.wikipedia.org/wiki/Atan2>
