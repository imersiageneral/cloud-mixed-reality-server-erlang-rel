# Introduction #

This folder contains pre-built version of the Imersia MRServer.

## Instructions ##

- If it doesn't exist already, create a folder called 'mrserver' at the root level, ie the same level as the http, https and ssl folders.

```bash
mkdir mrserver
```

- Copy the appropriate mrserver tar file to the mrserver folder:

```bash
cp ./mrserver-zip/ubuntu/mrserver-3.1.2.tar.gz ./mrserver
```

- Enter the mrserver folder

```bash
cd ./mrserver
```

- Extract the zip file

```bash
tar -zxvf ./mrserver-3.1.2.tar.gz
```
