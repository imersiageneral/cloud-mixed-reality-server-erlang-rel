"""
Created on Thu Jan 19 10:21:30 2017

A simple python script to throw some load at an MRServer by creating masses of Geobots.

@author: roycdavies
"""

# ----------------------------------------------------------------------------------------------------
# Import the modules
# ----------------------------------------------------------------------------------------------------
import requests
import json
import urllib
import websocket
import threading
import ssl
import time
# ----------------------------------------------------------------------------------------------------



# ----------------------------------------------------------------------------------------------------
# Constants
# ----------------------------------------------------------------------------------------------------
#baseuri = "imersia.org"
baseuri = "localhost"
apiurl = "https://" + baseuri + "/api"
authurl = apiurl + "/sessions"
socketurl = "wss://" + baseuri + "/wotcha"
geohash = 'rcknm1d9rsvt'
developerid = 'Test'
useremail = 'roy@imersia.com'
userid = ''
threadpool = []
# ----------------------------------------------------------------------------------------------------



# ----------------------------------------------------------------------------------------------------
# Log into the Imersia API
# ----------------------------------------------------------------------------------------------------
def ImersiaLogin ():
    headers = {'useremail': useremail, 'password': 'Your Password', 'token': 'Fred', 'developerid': developerid, 'location': geohash}
    req = requests.get(authurl, headers=headers, verify=False)

    if (req.status_code == 200):
        returndata = json.loads(req.text)
        return returndata["sessionid"]
    else:
        return ""
# ----------------------------------------------------------------------------------------------------



# ----------------------------------------------------------------------------------------------------
# Connect to the socket for this user
# ----------------------------------------------------------------------------------------------------
def on_recv(ws):
    while True:
        message = ws.recv()
        if (message == "ping"):
            ws.send("pong")
        else:
            print(message)
    return

def ConnectSocket (userid):
    ws = websocket.WebSocket(sslopt={"cert_reqs": ssl.CERT_NONE})
    ws.connect(socketurl + "/" + userid)
    t = threading.Thread(target=on_recv, args=(ws, ))
    t.start()
    return ws

def WotchChannel (ws, channelid):
    print (channelid)
    details = {"command": "wotcha", "sessionid": sessionid, "parameters": {"id": channelid}}
    ws.send(json.dumps(details))
# ----------------------------------------------------------------------------------------------------



# ----------------------------------------------------------------------------------------------------
# Send a GET request to the Imersia API
# ----------------------------------------------------------------------------------------------------
def GetCommand (command, headers):
    headers.update({'sessionid' : sessionid, 'developerid' : developerid, 'location': geohash})
    req = requests.get(apiurl + "/" + command, headers=headers, verify=False)
    if (req.status_code == 200):
        return {'status':req.status_code, 'result':json.loads(req.text)}
    else:
        return {'status':req.status_code, 'result':{}}
# ----------------------------------------------------------------------------------------------------



# ----------------------------------------------------------------------------------------------------
# Send a POST request to the Imersia API
# ----------------------------------------------------------------------------------------------------
def PostCommand (command, headers, body):
    headers.update({'sessionid' : sessionid, 'developerid' : developerid, 'location': geohash})
    req = requests.post(apiurl + "/" + command, headers=headers, json=body, verify=False)
    if (req.status_code == 200):
        return {'status':req.status_code, 'result':json.loads(req.text)}
    else:
        return {'status':req.status_code, 'result':{}}
# ----------------------------------------------------------------------------------------------------



# ----------------------------------------------------------------------------------------------------
# Send a POST request to the Imersia API
# ----------------------------------------------------------------------------------------------------
def DeleteCommand (command, headers):
    headers.update({'sessionid' : sessionid, 'developerid' : developerid, 'location': geohash})
    req = requests.delete(apiurl + "/" + command, headers=headers, verify=False)
    if (req.status_code == 200):
        return {'status':req.status_code, 'result':json.loads(req.text)}
    else:
        return {'status':req.status_code, 'result':{}}
# ----------------------------------------------------------------------------------------------------



# ----------------------------------------------------------------------------------------------------
# Main Function
# ----------------------------------------------------------------------------------------------------
sessionid = ImersiaLogin ()

if (sessionid == ""):
    print ("Error logging in")
else:
    print ("Logged in - sessionid is : " + sessionid)

    answer = GetCommand('user', {'useremail': useremail})
    print(answer)
    if (answer['status'] == 200):
        result = answer['result']
        userid = result['userid']
        print ("userid is : " + userid)

        create_channel_resp = PostCommand("channels", {"userid": userid}, {"name": "geobots"})
        channel = create_channel_resp['result']
        channelid = channel["channelid"]

        longitude = -180
        while (longitude <= 180):
            latitude = -90
            while (latitude <= 90):
                name = "Point " + str(latitude) + " | " + str(longitude)
                print(name)
                create_resp = PostCommand("geobots", {"channelid": channelid}, {"name": name, "location":{"latitude":latitude, "longitude":longitude}})
                latitude = latitude+10
            longitude = longitude+10

        time.sleep(30)

#            DeleteCommand("channels", {"channelid":channelid})

        for ws in threadpool:
            ws.close()

    else:
        print("Error getting userid")
