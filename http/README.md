# The HTTP Folder

Place your unsecured website here.

Some useful tips:

- If you are using React to create websites, create the website in the usual way (eg my-app), then move the source folder to a different name (eg my-app_src), and create a symbolic link to the build folder inside the source folder with the original name of the react app. That way, whenever you build the react app, the result is always ready to go.

```bash
npx create-react-app my-app
mv my-app my-app_src
cd my-app_src
yarn build
cd ..
ln -s ./my-app_src/build ./my-app
```

- Inside the react app folder, there is a file called package.json. Insert the following line to ensure it knows where the folder is in the directory structure:

```
"homepage": "/my-app",
```

- Further, if you want this app to be the default app, set the appropriate default in mrserver.toml, eg:

```
httpdefault = "my-app"
```
